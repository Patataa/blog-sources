+++
title = "Index"
+++

# Hello World! 

Welcome to my course materials. You can browse among them with the sidebar, the slides and courses are available.

Do not hesitate to ask further question and share your opinion and / or your suggestion to improve this website at: cedric.cf.edu[at]gmail.com

I wish you a good visit,  
Cédric Charière Fiedler