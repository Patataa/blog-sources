+++
title = "HPC"
weight = 4
+++

## About

This course has been written for the 3rd year students of ISIMA (Institut Supérieur D'Informatique, de Modélisation et de leurs Applications, a french school of IT engineering in Clermont-Ferrand).

## Context

This lesson is a synthesis of several books and talks. 
All the references will be given. They are suggestions for your further reading.

A great thanks to David HILL for her lesson material: this course is a fork
of her previous course.

## The assessments

- Lab participation
- A short survey
  - Literature review (3 pages)
- Final exam:
  - Theoretical and technical questions (from lab sessions)

  High Performance Computing summary.
You can find here all the sections with the slides.
The content is not fixed and it could be subject to changes.


## Topics

- History and Context
- Applications
- Theory and Modelization
- Parallel skeletons 
- Programming and Models
- Exascale and benchmarking
- Reproducibility and Floating point representation
- HPC and Cloud
- GP-GPU and hybrid computing
