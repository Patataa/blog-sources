+++
date = "2017-04-24"
draft = false
title = "1. HPC: History and Context"
excerpt = "Slides: HPC courses: Expectations"
slug = "slide-hpc-history"
tags = ["expectations","expectations"]
categories = [ "slide" ]
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = ""           # set "main" to add this content to the main menu
type= "slide"
theme = "white"
[revealOptions]
controls= true
progress= true
history= true

+++
<section data-transition="slide" data-background-image="" data-background-transition="concave">
	<h1>HPC</h1>
	<h2>History and Context</h2>
	<br/>
	<p>A fork of David Hill materials</p>
	<p> by Cédric Charière Fiedler - 2017</p>
</section>

---

## About computation

___

### What is a CPU?

- A FPU: Floating Point Unit
- A Core: a compute unit with its own FPU and Register
- A Socket: the chip 

___

### Flops: Meaning

- Floating-point operations per second
- Measure of computer performance
- FLOPS = sockets * (cores / sockets) * clock_frequency * (FLOPS / cycle)
- Quad-core PC at 2.5 GHz: 40 billion FLOPS = 40 GFLOPS

___

## The limitation of sequential computing

Some ways to improve the speed:

- Reduce the size of connection between the components: at quantic size, quantic issues
- Increase the clock frequency: increase energy consumption, heating the chips too much
- Share the computation with more compute units => **Parallelization**
---

## Parallel processing?

___

### Scalar processors

Each instruction executed by a scalar processor typically manipulates one or two data items at a time.

___

### Vector processors

Each instruction operates simultaneously on many data items

___

### Superscalar processor

Sort of a mixture of Vector and Scalar processors.

Each instruction processes one data item.

There are multiple redundant functional units within each CPU 

=> multiple instructions can be processing separate data items **concurrently**

___
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Superscalarpipeline.svg/978px-Superscalarpipeline.svg.png" height="400vh"/>
<br/>
Superscalar pipeline *.cf Wikipedia*
<small>
IF = Instruction Fetch, ID = Instruction Decode, EX = Execute, MEM = Memory access, WB = Register write back, i = Instruction number, t = Clock cycle [i.e., time])
</small>

___

### How does the pipeline work?

- “early superscalar” CPUs had two ALUs and a single FPU, 
- A modern design can include four ALUs, two FPUs, and two vector units. 
- If the dispatcher is ineffective at keeping all the units fed with instructions, the performance of the system will suffer.”


---

## Supercomputing

___

### Best supercomputer vs Yours

<table>
	<tr>
		<th></th>
		<th>Cores</th>
		<th>Memory</th>
		<th>Power</th>
		<th>Performance</th>
	</tr>
	<tr>
		<td>Yours</td>
		<td>[2;6]  (2.5 GHz)</td>
		<td>[4;32] GB</td>
		<td>[500;750] W</td>
		<td>~[20; 60] GFLOPS</td>
	</tr>
	<tr>
		<td>Sunway TaihuLight</td>
		<td>10,649,600 (1.45GHz)</td>
		<td>1,310,720 GB</td>
		<td>15,371.00 kW</td>
		<td>93,014.6 TFlops</td>
	</tr>
</table>

___
## Sunway TaihuLIght

![supercomputer](https://www.top500.org/static//featured/TaihuLight.jpg)

___

## Tianhe 2

![supercomputer](https://www.top500.org/static/media/uploads/blog/tianhe-2-jack-dongarra-pdf.jpg)

___

## Piz Daint

![supercomputer](http://insidehpc.com/wp-content/uploads/2013/09/Piz_Daint_left_person_LR.jpg)

___

## Supercomputers list

![top 500](https://www.top500.org/static//images/Top500_logo.png)

- A benchmark and ranking for the world's fastest super computers
- Link: [Top500 http://www.top500.org](http://www.top500.org)
- Since 1993, a list of computers ranked by their performance on the [LINPACK Benchmark](https://www.top500.org/project/linpack/)

___
## Some facts (November 2017)

- #1 and #2 are installed in China, #3 Switzerland, #4 Japan, #5 USA
- USA: 143, China: 202, Europe: 93
-  the No. 2 system, Tianhe-2, the No. 7 Trinity, the No. 8 Cori, and the No. 9 Oakforest-PACS uses Intel Xeon Phi processors to speed up their computational rate. The No. 3 system Piz Daint and the No. 5 system Titan are using NVIDIA GPUs to accelerate computation.
- Intel: 94.2% of TOP500 systems



---

### Supercomputing applications 1/2

- **Weather forecasting**, Satellite image analysis
- **Fluid dynamics** (such as modeling air flow around airplanes or automobiles)
- **AI**

___
### Supercomputing applications 2/2

- **Data-Mining**
- **Simulations of nuclear applications**
	- Vast number of variables and equations
- For many **stochastic applications** (more than 50% supercomputing usage)

___

## Example: a piece of History 
### [Earth Simulator](https://www.top500.org/featured/systems/the-earth-simulator-earth-simulator-center/)
Ranked Top 500 #1 - 2002 – 2004

<img src="/images/hpc/history/earth-simulator.jpg"/>

___

- Built by NEC, 640 nodes with 8 vector processors and 16 GB of memory at each node.
- Total: 5120 processors, 10 TB of memory
- Running global climate models to evaluate the effects of global warming and problems in solid earth geophysics

___

**Earth Simulator**

![earth_simulator](https://www.top500.org/static/media/uploads/20/top-1-systems/the-earth-simulator-earth-simulator-center/Earth_simulator_ES2_800x600.jpg)

___

**A dedicated building**

<img src="/images/hpc/history/es-building.jpg"/>
___

**EX: Ocean – Atmosphere Interactions**

<img src="/images/hpc/history/ocean-atmosphere-interactions.jpg" height="400vh"/>

The ocean warms the atmosphere, this generates winds which drive the ocean currents. 

___

**Deep current simulation in the Atlantic**

<img src="/images/hpc/history/deep-current-simulation.jpg" height="400vh"/>

Freon gas is transported from the north pole to the south by a deep littoral current


___

**In 2004?**

- 2 years later IBM's Blue Gene/L achieved about 36 TFLOPS. 
- It consumes 15 times less power per computation 
- 50 times smaller than Earth Simulator 1 

---

## History

___

### The Pre-History of "SC"

####  **IBM Naval Ordnance Research Calculator**
	
- The first machine generally referred to as a supercomputer (though not officially designated as one)
- It was used at Columbia University **from 1954 to 1963** to **calculate missile trajectories**. 
___
*IBM NORC*

![IBM NORC](https://upload.wikimedia.org/wikipedia/commons/9/9d/IBM_NORC.jpg)

___
#### Facts:

- It predated microprocessors
- clock speed of 1 microsecond 
- about **15,000 operations per second** (+, -,  / , * ).
- At the presentation ceremony, it calculated pi to 3089 digits, which was a record at the time

___

### Seymour Cray
#### The begininng of "SC" architecture

- Design of the first officially supercomputers for Control Data in the late 1960s
- His first design, CDC 6600
	- **pipelined scalar architecture** 
	- use of the **RISC instruction set** that his team developed

___

**CDC 6600**

![cdc 6600](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/CDC_6600_introduced_in_1964.jpg/800px-CDC_6600_introduced_in_1964.jpg)

___

- In this architecture: a single CPU overlaps fetching, decoding and executing instructions to process one instruction each clock cycle!
- Evolution of this machine included multi-processors

___

### From Multiprocessors to vector processors

- In 1972 Cray started his own company, **Cray Research**. He abandoned the multiprocessor architecture in favour of vector processing (this unrolling “for” “do” loops

___

### Facts

- Using a CDC 6600, the European Centre for Medium-Range Weather Forecasts (ECMWF) produced a 10-day forecast in 12 days. 
- Using one of Cray Research's first products, the Cray 1-A, the ECMWF was able to produce a 10-day forecast in five hours.

___

*Seymour Cray and a Cray-1*
![seymour cray and cray 1](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Seymour_Cray.jpg/800px-Seymour_Cray.jpg)


---
![illustration](/images/hpc/history/security.jpg)

## National Security

___

### Protectionism

- In their early history, the production and use supercomputers was carefully controlled, since they were used in critical nuclear weapons research. 
- They were also a source of national pride, symbolic of technical leadership. 
- Anti-dumping legislation was brought to bear against the importation of Japanese supercomputers in the US 
- It was revoked in 1998


___

- The first Bush administration (1990) defined supercomputers as being able to perform more than 195 Millions of Theoretical Operations per Second (MTOPS). 
- Anyway by 1997, ordinary microprocessors for PCs were capable of over 450 MTOPS.
- Technologists continued to increase the performances of massive parallel supercomputers. 
- Peripheral speeds had increased so that I/O was no longer a bottleneck. 
- High-speed communications made distributed and parallel designs possible.


---

## Tomorrow?

___

### The place of GPU

- Massive vector processor
- Better in computation with less power usage
- Need dedicated algorithms with specific considerations
	- GPU architecture != CPU architecture
	- High entry cost (to learn)

___

### Quantum computing

- IBM and the first 50 QBits Quantum computer
- APIs are available to test and develop (emulator + access to their 16 QBits computer)
	- [Link to the QISkit](https://developer.ibm.com/code/2017/05/17/developers-guide-to-quantum-qiskit-sdk/)
- Security issue? 
	- Need of Post Quantum Cryptography
