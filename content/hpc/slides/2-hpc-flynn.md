+++
date = "2017-04-24"
draft = false
title = "2. HPC: Parallel Architecture and processing elements"
excerpt = "Slides: HPC courses: Expectations"
slug = "slide-hpc-parallel-architecture"
tags = ["parallelization", "architecture", "flynn"]
categories = [ "slide" ]
#image = "/images/presentation-angular/angular-header.jpg"
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = ""           # set "main" to add this content to the main menu
type= "slide"
theme = "white"
[revealOptions]
controls= true
progress= true
history= true

+++

<section data-transition="slide" data-background-image="" data-background-transition="concave">
	<h3>Flynn Taxonomy</h3>
	<br/>
	<p>A fork of David Hill materials</p>
	<p> by Cédric Charière Fiedler - 2017</p>
</section>

---

## Flynn Taxonomy

Michael J. FLynn proposed in 1966 a classification of computer architectures based on a number of instruction and data streams that can be processed simultaneously

___

<table>
	<tr>
		<td></td>
		<td>Single Data</td>
		<td>Multiple Data</td>
	</tr>
	<tr>
		<td>Single Instruction</td>
		<td>SISD</td>
		<td>SIMD</td>
	</tr>
	<tr>
		<td>Multiple Instruction</td>
		<td>MISD</td>
		<td>MIMD</td>
	</tr>
</table>

___

![illustration](/images/hpc/flynn/four-classes.jpeg)
The four classes of the Flynn's taxonomy
___

### SISD: Conventional 'old' Computer

- Traditional CPU architecture:  at one time, only a single instruction is executed operating on a single data item
___

### MISD

- Many functional units perform different operations on the same data
- No architectures answering to this description
- Fault-tolerant computers

___

### SIMD

- Many units perform the same operations on different data
- Vector processing
- GPU, SSE, AVX...

___

### MIMD

- Many units perform different operations of different data
- Conventional multi-core CPU

---

## MIMD Architectures

___

### Shared Memory MIMD

- Communication: Source PE (Processing Element) writes data to the Shared Memory
and the destination PE get it
- OS: Conventional OSes can be adapted (up to a certain Scale)
- Limitation: memory (bandwidth), resilience (any memory component or processor failure affects the whole system)
- Atomicity and synchronization: more processors == more blocking states  
___

#### Symetric MultiProcessing (SMP)

- Multiprocessor computer hardware architecture
- Two or more identical processors are connected to a single shared main memory and are controlled by a single OS instance
- Most common multiprocessor systems today
- The multicore processors have an uniform memory access (shared cache, typically the L3 or L2 cache)

___

- Each processor executing different programs and working on different data (MIMD)
- They have the capability of sharing common resources (memory, I/O device, interrupt system...)
- There are connected using a system bus or a crossbar

___

![SMP](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/SMP_-_Symmetric_Multiprocessor_System.svg/994px-SMP_-_Symmetric_Multiprocessor_System.svg.png)

___

### Distributed Memory MIMD

- Inter-Process Communication via High Speed Network
- Network can be configured to meet different topologies: Tree, Mesh, Cube, Torus, ...

___
 
- easily / readily expandable
- Resilience
- Cluster and grid computing architectures are of this kind

___


![DMMIMD](/images/hpc/flynn/dmmimd.jpg)

___
### Shared Nothing MIMD (MPP)

Massively Parallel Processing

- Multiple processors work on different part of the program
- Each processors use its own OS and memory
- Communication: messaging interface

___
The setup for MPP is complicated: you have to tiles the processed data, in considering the hardware specifications, your algorithms and the available nodes to optimize the computation
___

- A MPP system is also known as a *loosely coupled* or *shared nothing* system
- It is considered better than a SMP for applications that **allow a number of data to be searched in parallel or to be split for the processing** (image processing, map/reduce approaches)

---

### SPMD - Common parallelism

- Single Program Multiple Data
- SPMD is a subcategory of MIMD architecture with a behavior close to SIMD
- SPMD might refer to multiple threads sharing an address space or multiple processes with their own address spaces sharing data through messages (or file system) 
