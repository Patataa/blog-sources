+++
weight = 10
date = "2017-07-21"
draft = false
title = "0. Expectations: HPC course"
slug = "expectation-hpc"
tags = ["hpc", "plan","presentation"]
categories = [ "hpc" ]
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = "hpc"           # set "main" to add this content to the main menu
+++

High Performance Computing summary.
You can find here all the sections with the slide.

---

## Some Context

This lesson is a synthesis of several books and talks. 
All the references will be given. They are suggestions for your further reading.

A great thanks to David HILL for her lesson material: this course is a fork
of her previous course.

---

## The course objectives

- Discovering the HPC ecosystem
- 
- ...

---

## The assessments

- Lab participation
- A short survey
  - Literature review (3 pages)
- Final exam:
  - Theoretical and technical questions (from lab sessions)