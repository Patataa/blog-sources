+++
weight = 10
date = "2017-07-21"
draft = true
title = "0. Plan : HPC"
slug = "plan-hpc"
tags = ["hpc", "plan","presentation"]
categories = [ "hpc" ]
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = "hpc"           # set "main" to add this content to the main menu
+++



## 0 - Context

- Current 

## I – Introduction to HPC main concepts

### Computing

- Context
    - Augmentation of the computation frequency
    - Current limitation of high frequency computation
        - Physical limits
            - Temperature
            - Size of connexion (nanometric)
        - Cost
    - Current solution: Parallelization
        - Divide and conquer strategy

- Flynn taxonomy
    - SISD
    - SIMD
    - MIMD

- Which type of algorithms ?
    - High volume of data to compute
    - High complexity

- Type of operations
    - Interactive computing
    - Batch operations
    - Tasks notions

### Storage

- Constraints
    - Use case: big volumes of data -> How to store mass volume ?
    - Bottlenecks: Bandwith
- Type of storages
    - HDD
    - SSD
    - -> Improvement axe: cost and energy consumption
- Some solution
    - Caches solutions
        - Require specific architecture
        - Increase the complexity of data management 

### Networking

- Constraints
    - Manage the data repartition
    - Communication of tasks
- Architectures taxonomy

### Software for HPC

- What do they purpose ?
- Some tools
    - Hadoop
    - Apache Spark
    - Tensorflow

### The case of local clusters

## II – Different distributed approaches for Different computing approaches

### P2P (peer to peer)

- Definition
- How that works ?
    - Architecture
- Use cases
    - Block Chains
    - Scientistic examples
- How to use ?
    - Some tools
- Pro
- Cons

### Cluster

- Definition
    - Constellation / Cluster
- How that works ?
    - Architecture
- Use cases
    - Block Chains
    - Scientistic examples
- How to use ?
    - Some tools
- Pro
- Cons

### « Grid Computing »

- Definition
- How that works ?
    - Architecture
- Use cases
    - Block Chains
    - Scientistic examples
- How to use ?
    - Some tools
- Pro
- Cons

### Internet Computing and desktop grids

- Definition
- How that works ?
    - Architecture
- Use cases
    - Block Chains
    - Scientistic examples
- How to use ?
    - Some tools
- Pro
- Cons

### Grids of Supercomputers

- Definition
- How that works ?
    - Architecture
- Use cases
    - Block Chains
    - Scientistic examples
- How to use ?
    - Some tools
- Pro
- Cons

### Cloud computing

- Definition
- How that works ?
    - Architecture
- Use cases
    - Block Chains
    - Scientistic examples
- How to use ?
    - Some tools
- Pro
- Cons

### Parasitic computing

- Definition
- How that works ?
    - Architecture
- Use cases
    - Block Chains
    - Scientistic examples
- How to use ?
    - Some tools
- Pro
- Cons

### Hybrid computing

- Definition
- How that works ?
    - Architecture
- Use cases
    - Block Chains
    - Scientistic examples
- How to use ?
    - Some tools
- Pro
- Cons

## III – Future generation of HPC, towards Exascale computing

<!-- To Improve -->
### Many cores


- Definition
- How that works ?
    - Architecture
    - Example: scalar product
- How to use ?
    - Explicit declaration
    - Some tools
- Current hardware
    - Xeon Phi
- Advantages
    - Faster than GPU for small operations
        - Reduction of communication cost
    - Implicit vectorisation operation
        - 
- Disadvantages
    - Current cost
    - Need of explicit declaration <!-- To valid -->
    - Specific code for specific system architecture <!-- To valid -->

### Use of hardware accelerator

- Out of Order flag
- Floating point heuristics

- Advantages
    - Perfomances improvement (show benchmarks)
    - 
- Consequences
    - The rounding of floating number: loss of precision
    - The loss of the reproductibility 



<!-- To Improve -->
### The case of GP-GPU

- Main differences between a CPU and a GPU
    - Explanation of the vectorisation notion
    - Hardware specifications of a GPU
    - 
- Use case
- Lifecycle of a GP-GPU algorithm

- GPU architecture
- GPU models
    - 
- Some tools
    - OpenCL
    - Cuda
    - Upper Level: Sycl, VexCL, Magma...
- The main constraints


### Hybridation of GP-GPU and CPU

- The need of Load Balancing
- Clusterisation

## References 

