+++
title = "OpenGL"
weight=6
+++

## À propos

Ce cours est à destination des troisièmes années de l'ISIMA.

Le contenu présenté ici n'est pas exhaustif et ne pourra être considéré comme étalon minimal de connaissance nécessaire au partiel final.

Le contenu présent ici s'inscrit en tant qu'enrichissement du support de cours de M. Malgouyres présent [ici](https://malgouyres.org/opengl).

<div class="alert alert-info">

    Les étudiants sont fortement invités à consolider leurs connaissances avec les lectures des références précisées au sein de chaque slide / support.
</div>