+++
weight = 80
date = "2018-12-19"
draft = false
title = "OpenGL - Mini exam - 2"
slug = "opengl-mini-exam-2"
tags = ["opengl","exam"]
categories = [ "opengl", "exam" ]
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
+++

Vous trouverez ci-dessous le sujet du deuxième mini-exam.
Vous avez 10 minutes top chrono pour le réaliser, une seule participation par personne (la première soumise sera la seule prise en compte).
Les soumissions seront refusées passées le délai des 10 premières minutes.
Toute page internet autre que ce QCM détectée sur vos machines durant les 10 minutes entraînera une note nulle.

Bon courage !

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLScAEi-BbP5OpGyzU7eZgXZ8fydifh3dul9G4nsSqIq_VNY6CA/viewform?embedded=true" width="100%" height="900" frameborder="0" marginheight="0" marginwidth="0">Chargement en cours...</iframe>