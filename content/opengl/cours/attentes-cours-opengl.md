+++
weight = 15
date = "2017-04-19"
draft = false
title = "0.1 Attentes du cours OpenGL"
slug = "attentes-opengl"
tags = ["attentes"]
categories = [ "opengl" ]
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = "ionic"          # set "main" to add this content to the main menu
+++

Les attentes du cours, livrables demandés et mode d'évaluation.

<!--more-->

## Projet OpenGL

<i class="fa fa-clock-o"></i> <strong>Durée</strong> : 20 heures de TP  
<i class="fa fa-user"></i>**Binôme de projet**  
<i class="fa fa-book"></i> **Sujet** : Création d'un rendu graphique OpenGL

En 10 séances de TP, vous serez amenés à développer un rendu graphique OpenGL.

Le sujet de l'application OpenGL est libre. Les attentes concernent les éléments réalisés, l'achitecture de code et la qualité du logiciel.

### Contraintes

Les points suivants sont considérés comme **impératifs**. Toute absence de ces éléments entraînera le retrait de points.

- Un Repo GIT distant (Gitlab, Github, BitBucket...)
- Documentation : le code doit être documenté
- Une page de rapport : `README.MD` indiquant les points suivants
    - Comment installer votre application pour le développement
        - Dépendances
        - Prérequis
    - Ce que vous avez réalisé (des phrases courtes)
    - Ce que vous n'avez pas réalisé et pourquoi
    - Les éléments supplémentaires ajoutés au projet

### Attentes minimales

Ces attentes sont considérées comme minimal et leur non implémentation entraînera une note en dessous de la moyenne.

- Diagramme UML de la solution
- Une explication de votre choix d'architecture (4000 mots max pour l'explication de la solution)
- Affichage d'une scène 3D contenant plusieurs objets
- Déplacement de la caméra dans la scène
- Eclairage Ambiant
- Eclairage Diffus et / ou spéculaire sur certains objets

### Attentes supplémentaires

Ces attentes sont **obligatoires**. Leur implémentation permet d'obtenir des points supplémentaires.

- Application de textures sur les Objets 3d
- Programmation par Shaders et VBO
- Light Mapping
- Ombres projetées par Shadow Mapping
- Chargement d'une scène 3D externe avec Assimp
- Post-Processing du rendu d'une scène