+++
date = "2018-11-23"
draft = false
title = "Introduction aux Shaders"
excerpt = "Introduction aux Shaders"
slug = "opengl-shaders-introduction"
tags = ["opengl","shaders","introduction"]
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = ""           # set "main" to add this content to the main menu
+++

[Lien vers la présentation](/pdf/shaders_introduction.pdf)
