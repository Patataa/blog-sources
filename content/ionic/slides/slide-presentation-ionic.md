+++
date = "2017-04-21"
draft = true
title = "1. Slides : Présentation de Ionic - Slide"
excerpt = "Slides : Présentation brève de Ionic, Angular et Cordova"

slug = "slide-presentation-ionic"
tags = ["ionic","cordova","presentation"]
categories = [ "slide" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = ""           # set "main" to add this content to the main menu

type= "slide"
theme = "white"
[revealOptions]
controls= true
progress= true
history= true
center= true
+++


<section data-transition="slide" data-background="#2E88FF" data-background-transition="concave">
	<h1> Présentation de Ionic</h1>
	<blockquote>Ou comment créer rapidement des applications mobiles hybrides</blockquote>
	<br/>
	<p>2017 - Cédric CHARIERE FIEDLER</p>
</section>

---

<!-- .slide: data-background="#2E88FF" -->

<h2> <i class="fa fa-book"></i> Définitions</h2>

___

### Une application native

- Application **développée spécifiquement pour un système d'exploitation**
- **Les +** : Configuration plus fine, plus grande maîtrise du logiciel
- **Les -** : Long à développé si plusieurs systèmes visés

___

### Une application web

- Application développée en HTML accessible et utilisable depuis un navigateur internet.
- **Les +** : 
  - Ne nécessite qu'un navigateur web
  - Pas besoin de télécharger l'appli
- **Les -** :
  - Pas d'accès au play Store
  - Plus lent qu'une application native
  - Accès restreints aux fonctions natives

___
### Une application hybride 1

> **Application web** s'exécutant sur une **webview locale** ayant des accès à une **brique native** intéragissant avec le système d'exploitation



___
### Une application hybride 2

- **Les +**:
  - Plus rapide à développer (1 seul code pour plusieurs systèmes d'exploitation)
  - Possibilité d'utiliser du code web déjà développé
- **Les -**:
  - Plus lent qu'une application native
  - Des accès aux fonctions natives parfois compliquées
  

___

### Références

- [**Ionic Documentation overview, Ionic**](http://ionicframework.com/docs/v1/overview)
- [**Native, HTML5, or Hybrid: Understanding Your Mobile Application Development Options**, developer Salesforce](https://developer.salesforce.com/page/Native,_HTML5,_or_Hybrid:_Understanding_Your_Mobile_Application_Development_Options)
- [](https://www.mobiloud.com/blog/native-web-or-hybrid-apps/)
- [**Application mobile native, webapplication ou hybride: 3 solutions !**, La Treebu](http://www.latreebu.com/blog/application-mobile-native-ou-web-3-solutions/)
- [**Application mobile native, web ou hybride ?**, Supinfo](http://www.supinfo.com/articles/single/145-application-mobile-native-web-hybride)

---

<!-- .slide: data-background="#2E88FF" -->
<h2> <i class="fa fa-mobile"></i> L'écosystème Ionic</h2>

___
### En bref

- Framework pour la création d'applications hybrides
- Racine de projet fonctionnel
- Ecosystème cloud
  - De nombreux services
  - Prototypage : **Ionic Creator**
  - Partage de prototype : **Ionic View**
___

### Framework Ionic

- Conception d'applications hybrides en Typescript-JS
  - Composants natifs : Ionic == surcouche **Cordova**
  - Composants web : **Angular** et composants personnalisés
- Un thème graphique initial : **Material Design**


___

*Exemples d'applications Ionic*

![Exemples d'applications Ionic](/images/presentation-ionic/ionic-example.jpg)


___
*Framework Ionic*
![Une Application Ionic - Cordova](/images/presentation-ionic/ionic-structure.jpg)
___

### Framework Ionic - Les plugins

- Plugins natifs: Interface native / JS
  - **Le code de chaque plateforme est codé en langage natif** (un par plateforme)
  - Cordova : Encapsulation dans des **interfaces JS**
  - Ionic : Encapsulation des interfaces Cordova dans des **composants Angular**
- Webview
  - Utilisation des composants Angular
  - Utilisation des fonctionnalités natives du navigateur (Micro, caméra...)

___
### La graine de projet, déjà opérationnelle

S'appuie sur les bases Cordova + surcouche [ionic-app-script](https://github.com/driftyco/ionic-app-scripts)

- Structure initiale de projet
- Générateur de code (pages, services...)
- Tasks runner (minification, transpilation, compression, linter...)

<div class="alert alert-info" role="alert">
  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
  Les outils de test et de documentation sont à configurer manuellement
</div>

___
### Prototypage : Ionic Creator

![Capture D'écran de Ionic Creator](/images/presentation-ionic/ionic-creator.jpg)

___

### Prototypage : [Ionic Creator](https://creator.ionic.io)

- Pratique pour **prototyper**
- **La plupart des options sont payantes** (dont la conversion HTML)
- Pas de gestion des composants personnels
- **Non OpenSource** (outil non modifiable)

___
### Partage de prototype : Ionic View

![Capture d'écran de Ionic View](/images/presentation-ionic/ionic-view.jpg)

___
### Partage de prototype : Ionic View

- Développement : **partages et mises à jours de plusieurs applications**
- Pas besoin d'inscription sur les stores
- **Cas d'utilisation : retour client, collègues et testeurs**

<div class="alert alert-warning" role="alert">
  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
  Gratuit jusqu'à un certain volume d'utilisation (nombre de mobiles, volume d'upload...)
</div>

___
### Services Cloud

- Système de mise à jour
- Authentification centralisée avec providers Google, facebook...
- Notifications Push pour les utilisateurs
- Compilation multi-plateformes distante

<div class="alert alert-warning" role="alert">
  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
  Gratuit jusqu'à un certain volume d'utilisation (nombre de mobiles, volume d'upload...)
</div>

-- 

### Market

- Projets starters
- Plugins 
- Thèmes graphiques


<div class="alert alert-warning" role="alert">
  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
  Les licences d'utilisation ne sont pas mises en valeur, attention lors des achats !
</div>

___

### Conclusion

** Ionic ==**

- Framework de création d'applications mobiles hybrides
- Des bases de projet quasi-complets + utilitaires
- Des outils de prototypages
- Des services cloud (compilation, notification, authentification...)

___

### Références

- [Ionic Creator](https://creator.ionic.io)
- [Ionic View](https://docs.ionic.io/tools/view/)
- [Ionic Cloud](https://ionicframework.com/products)
- [Market](https://market.ionic.io)
---
<!-- .slide: data-background="#2E88FF" -->
<h2> <i class="fa fa-question"></i> Des questions <i class="fa fa-question"></i> </h2>
