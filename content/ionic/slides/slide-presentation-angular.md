+++
date = "2017-04-24"
draft = true
title = "2. Slides : Présentation d'Angular"
excerpt = "Slides : Présentation brève d'Angular"

slug = "slide-presentation-angular"
tags = ["angular","presentation"]
categories = [ "slide" ]
image = "/images/presentation-angular/angular-header.jpg"
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = ""           # set "main" to add this content to the main menu

type= "slide"
theme = "white"
[revealOptions]
transition= 'concave'
controls= true
progress= true
history= true
center= true
+++

<section data-transition="slide" data-background="#C3002F" data-background-transition="concave">
	<h1>Présentation d'Angular</h1>
	<blockquote>Le framework Frontend survitaminé</blockquote>
	<br/>
	<p>2017 - Cédric CHARIERE FIEDLER</p>
</section>

---
<!-- .slide: data-background="#C3002F" -->

<h2> <i class="fa fa-book"></i> En bref</h2>

---

### A quoi sert Angular  ?

___

#### Structurer une application client

- Architecture MV* > la page HTML / PHP et les cinquantes fichiers JS
- Plus facile à maintenir
- Notion de [design modulaire](https://designshack.net/articles/layouts/modular-design-the-complete-primer-for-beginners/)

___
#### Faciliter le développement d'interface client riche

- Templates HTML Dynamiques
- Asynchronisme par défaut (Observable / Promise)
- Liaison des données simples

___ 

#### Notion de *Single Page App*

- Une page chargée gérant en interne le routage, les autorisations...
- Possibilité de *lazy loading* de module (certaines parties ne sont chargées que quand nécessaire)

---

### Angular.JS / Angular 2-4 ?

___

#### Etat des lieux

- **Angular.JS** : version 1, encore maintenue
- **Angular 2** : La nouvelle version, toute propre
- **Angular 4** : Passage au [*semantic versioning*](http://semver.org/)

___

#### Numéro de version 

<big><strong>
  <span class="text-info">1</span>.
  <span class="text-success">2</span>.
  <span class="text-danger">3</span>
</strong></big>

1. Version <span class="text-info"><strong>MAJEUR</strong></span> quand l'api fait des **changement incompatible** avec la version précédente
2. Version <span class="text-success"><strong>MINEURE</strong></span> quand des **fonctionnalités sont ajoutées** et conserve la compatibilité avec la version précédente
3. Version <span class="text-danger"><strong>PATCH</strong></span> pour les **corrections de bugs**.

___

<div class="alert alert-success"> 
  Angular.JS <strong>!=</strong> Angular[2,3,4...]
</div>

---

### Typescript

[Lien vers la documentation](https://www.typescriptlang.org/)

___

#### Meta-langage Javascript

- Ajoute des fonctionnalités au JS (en plus d'être compatible avec les prochaines encore non implémentées)
- Propose une structure d'utilisation plus strict
- Est compilé en JS

___

#### OpenSource
  - Maintenu par Microsoft, utilisé chez Google
  - Utilisé par Ionic, Angular, Aurelia...
  - S'est imposé par rapport à CoffeeScript
___
#### Notion de typage

- Contrôle des erreurs à la compilation
- Les IDE permettent ainsi l'auto-complétion
- Documentation plus lisible

___

#### Interfaces, enums, décorateurs...

  - Syntaxe Objet
  - Décorateurs : métadonnées permettant d'intéragir avec le code (lecture / écriture) à la compilation

___

#### Prérequis

<div class="alert alert-warning">**Attention !** : ajoute des étapes nécessaires dans la transformation du code source</div>
<br>

- Compilation du code TS <i class="fa fa-long-arrow-right"></i> JS
- Utilisation de fichiers de correspondance TS <i class="fa fa-arrows-h"></i> JS pour le deboggage (fichiers *map*)

___
*Exemple de classe Typescript*

```typescript
import {Injectable} from "@angular/core";
import {UserSegment} from "../model/user-segment";
import {Story} from "../../story/model/story";
import {Recorder} from "./recorder";
import {RecordFileManager} from "./record-file-manager.service";
import {UserSegmentService} from "./user-segment.service";

@Injectable()
/**
 * Record Service
 * Abstraction of story recording: the service manages the data story
 * and the files persistence.
 */
export class RecordService {

  private _recorder: Recorder;
  private _data: Array<Blob>;

  constructor(
    private userSegmentService: UserSegmentService,
    private recordFileManager: RecordFileManager
  ) {
    this._recorder = new Recorder();
  }

  public get data() {
    return this.data;
  }

  public async newSession(story:Story) {
    let currentUserSegment: UserSegment = await this.userSegmentService.create(story.segments[0]);
    this._data = [];
  }
  // ...
}
```


___

#### Conclusion

<div class="alert alert-warning">
Nécessite cependant une suite utilitaire particulière pour son utilisation.
</div>
<div class="alert alert-success">
Facilite grandement l'utilisation de JS et permet de créer des applications plus robustes.
</div>

---
<!-- .slide: data-background="#C3002F" -->
## L'architecture d'Angular

___

### Une architecture MV*
<!-- Diagramme de la structure MV* -->

![Structure d'Angular](/images/presentation-angular/structure-angular.jpg)

---
### Les composants
___

#### Une brique modulaire

**Un composant == une brique de Lego** avec son propre comportement et son propre style

- Un composant **contrôle une partie de l'écran** appelé une *vue*
- Exemple : Une map Google, une vidéo youtube, un lecteur audio...

___
#### Imbrication récursive

- Les composants peuvent **s'imbriquer les uns dans les autres**
  - Ex. : Un lecteur audio imbrique des boutons

![Imbrication de composants](https://angular.io/resources/images/devguide/architecture/component-tree.png)
<center>*Imbrication de composants*, cf. doc Angular officielle</center>

___
#### Forme d'un composant

- Chaque composant a :
  - un identifiant `<my-id></my-id>`
  - des attributs `<my-id attrib="smthg"></my-id`
  - Ex. : Lecteur Audio: `<audio></audio>`

___

#### Exemple

```
<audio controls>
  <source src="horse.ogg" type="audio/ogg">
  <source src="horse.mp3" type="audio/mpeg">
Your browser does not support the audio element.
</audio>
```


- Le composant `<audio>` a pour identifiant `audio`.
- Son attribut `controls` active la présence des boutons de contrôle.
- Les sous-composants `<source>` décrivent les liens vers les fichiers audios.

___

#### Un composant **Angular**

Un composant Angular est la synthèse entre :

- Un **template HTML**, pouvant imbriquer d'autres composants
- Une **feuille de style CSS/SCSS**
- Un **contrôleur .TS/.JS** pouvant être lié à **plusieurs services**.


---

### Les Directives

___

#### Page HTML : Affichage dynamique

- L'affichage d'une page HTML avec Angular est réalisé de **manière dynamique**.
- Le DOM est construit en respectant les **instructions décrites par les directives**.

___

#### @Directive()

- Une directive est définie par le décorateur `@Directive`.
- Un composant décrit par `@Component` est une directive avec un template.

- Il existe deux types de directives : **structurelles et attributs**

___

#### Directives structurelles

**Elles modifient la structure d'un template HTML** en ajoutant, retirant, modifiant des **éléments du DOM**.


*Exemple*

```
<li *ngFor="let story of stories"></li>
<story-detail *ngIf="selectedStory"></story-detail>
```

___
#### *ngFor

<br>
```
<li *ngFor="let story of stories"></li>
```
<br>
`*ngFor` génère autant d'éléments `<li>` qu'il y a d'objets dans le tableau `stories`

___
#### *ngIf

<br>
```
<story-detail *ngIf="selectedStory"></story-detail>
```
<br>

`*ngIf` affichera la composant `<story-detail>` uniquement quand la valeur de `selectedStory` existera

___ 

#### Directives dynamiques

Par défaut, **toute modification** des valeurs référentes (`stories` et `selectedStory`) entraîne une **reconstruction du DOM** et un **rafraîchissement de la vue**.

___

#### Directives attributs

- **Elles modifie l'apparence ou le comportement d'éléments existant**. 
- Elles ressemblent à des attributs HTML.

___

#### Directive attribut != attributs d'un composant

<div class="alert alert-warning">
Une directive attribut ne correspond pas aux attributs déjà existants d'un composant !
</div>
<div class="alert alert-success">
 Au contraire, elle permet d'ajouter des mécaniques supplémentaires.
</div>
___

#### Exemples

Les directives `ngSwitch`, `ngStyle` et `ngClass` modifient l'aspect d'éléments du DOM et des composants.

```
<div [ngClass]="currentClasses">This div is initially saveable, unchanged, and special</div>
```

___

#### Marqueurs de liaison de données

Ce type de directive est reconnaissable par la présence des marqueurs de liaison de données : `[]` |& `()`


___

#### Conclusion

- Les **directives structurell**e permettent de **modifier le DOM**
- Les **directives attributs** permettent d'**augmenter/modifier le comportement de composants**

---

### Les Services

___
#### Définition

<div class="alert alert-info">
  <i class="fa fa-book"></i> Angular ne propose aucune définition propre d'un service.
</div>

<i class="fa fa-long-arrow-right"></i> Libre au dévelopeur de définir son utilité

___
#### **Factorisation de code métier**
  - **Tout code** présent et **dupliqué** entre deux composants **doit être centralisé
dans un service**
  - La taille des contrôleur doit être la plus petite possible.

<br/>

<div class="alert alert-info">
	<i class="fa fa-check-square-o" aria-hidden="true"></i> Externalisation le plus possible du comportement des contrôleurs dans un service
</div>

___

#### **Injectable** 

- Un service peut être utilisé par d'autres services / composants
- Résolue par le **système d'injection**
- Une **instance de service est commune** pour tous les éléments dépendants du même niveau hierarchique d'injection
- Utilisable pour **mutualiser des données entre composants**

___

#### **Exemples de services**

  - Service d'authentification
  - Persistence des données
  - Calculs de taxes
  - ...

___

#### Conclusion

- **Centraliser le code métier** dans un service
- **Minimiser la taille** des contrôleurs / composants
- Les dépendances sont **résolues par l'injecteur** Angular
- Peut être utiliser pour **mutualiser des données** entre composants

---

### Les Modèles

___

#### <i class="fa fa-book"></i> Définition

- Structure des données utilisées par l'application
- Ne contient pas de code métier

___

####  **Une classe Typescript** /  JS

- Approche objet
- Sucre syntaxique uniquement

___

#### Exemple d'une classe typescript

```typescript
import {Segment} from "./chapter";
export class Story {
  private _title:string;
  private _chapters:Array<Chapter>;

  constructor(title?: string, chapters?: Array<Chapter>) {
    this._title = title;
    this._chapters = [];
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }
  // ...
```

___

#### **Une interface**
  
- **Décrit** le type des attributs / signature des fonctions d'un ***Objet* javascript**.
- <span class="text text-warning">Ne propose **pas de comportement** pour les objets.</span>
- Utilisable pour décrire **un objet javascript** :
	- **construit dynamiquement**
	- **obtenu par déserialisation** par exemple

___

#### Exemple d'une interface typescript

```typescript
import {iChapter} from "./ichapter";

export interface iStory {
  title: string;
  chapters: Array<iChapter>;
}

```

---

### Pipes

___
#### Définition

- Permettent de **transformer dynamiquement les valeurs** affichées dans un template
- **Chaînables**

___

#### Exemple de l'utilisation d'un pipe

```
  price | currency:'USD':true
```

Affiche le prix 42.33 sous la forme `$42.33` [cf. doc](https://angular.io/docs/ts/latest/guide/pipes.html).

---

### Références

- [**Web Components**, MDN](https://developer.mozilla.org/en-US/docs/Web/Web_Components)
- [**< audio >**, MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio)
- [**Architecture Overview**, Angular doc](https://angular.io/docs/ts/latest/guide/architecture.html)

---
<!-- .slide: data-background="#C3002F" -->
## Les composants
---

### <i class="fa fa-book"></i> Définition
Un composant web est la synthèse

- d'un **comportement** (component): TS/JS
- d'une **mise en forme** (template): HTML
- d'une **feuille de style** : css/scss

___
        
### Le contrôleur TS / JS, *component*

- Liaison entre les services et l'interface graphique
- Code uniquement du comportement d'une page
- Exposition de variables et de fonctions pour la page

___

### Code type d'un contrôleur

```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'pause-btn',
  templateUrl: 'pause-btn.html',
  styleUrl: 'pause-btn.scss' 
})
export class PauseBtn {
  protected _paused: boolean;
  
  constructor() {
    this._paused = false;
  }

  get paused():boolean {
    return this._paused;
  }

  pause():boolean {
    this._paused = true;
    return this.paused;
  }
}
```

---

### Le template HTML

___

#### <i class="fa fa-book"></i> Définition

- **Décrit la structure du DOM** du composant
- Peut être composé de **balises HTML standards, de composants, de directives**
- Peut utiliser des **pipes** pour modifier l'affichage de certaines données.

- **Est lié dynamiquement à des variables et des fonctions du contrôleur** du composant auquel il est associé.

___

#### Exemple de template

```
<ion-content padding>
  <h2>{{userVersion.story.title}}</h2>
  <p>{{userVersion.recordDate}}</p>
  <ion-img width="100%" height="80%" src="assets/img/thumbnail-totoro.png"></ion-img>
</ion-content>

<ion-footer>
  <ion-toolbar >
    <ion-item>
      <audio controls>
        <source [src]="getSegment(userVersion)" type="audio/wav">
        Your browser does not support the audio element.
      </audio>
    </ion-item>
  </ion-toolbar>
</ion-footer>
```

___

#### Les Expressions

- Encadrées par des `{{ myExpression }}`
- Permettent d'**injecter de manière dynamique le contenu d'une variable** au sein d'une balise HTML. 

___

#### Les Expressions: exemple

```
<h1>{{title | uppercase}}</h1>

```
- Le contenu de la variable `title` décrite dans le contrôleur Typescript associé, sera **insérée dans la balise** `<h1>` et sera **mise en majuscule** automatiquement. 

- **Toute modification** de la variable `title` entraînera le **rafraîchissement de la vue**.

___

#### Les attributs HTML

**Template <i class="fa fa-long-arrow-left"></i> contrôleur**

- `[attrib]` associe la valeur d'un attribut HTML à une variable ou une fonction

```
<img [src]="getImageSrc()" />
```

- L'attribut `src` de la balise `<img>` est alors associé au résultat de la fonction `getImageSrc()`. 

___

#### Résultat dynamique

Si la fonction retourne à l'instant **t+1** un résultat différent de l'instant **t**, alors la valeur de l'attribut `src` est modifié.  


___
<div class="alert alert-success">
  Les signes <strong>[]</strong> indiquent une liaison de données depuis le contrôleur vers le template.
</div>

___
#### Les attributs HTML

**Template <i class="fa fa-long-arrow-right"></i> contrôleur**

- `(event)` associe l'action d'un utilisateur sur un élément DOM à une variable ou une fonction du Contrôleur

``` 
<img src="..." (click)="imgAction()"/>
```
- Clic sur l'image =  le déclenchement de la fonction `imgAction()` décrite par le contrôleur typescript.

___

<div class="alert alert-success">
  Les signes <strong>()</strong> indiquent une liaison de données depuis le template vers le contrôleur.
</div>

---

#### Conclusion

- Template construits de manière dynamique selon les expressions, composants, directives et pipes
- Liaisons de données :
  - Template <i class="fa fa-long-arrow-left"></i> contrôleur : `[]`
  - Template <i class="fa fa-long-arrow-right"></i> contrôleur : `()`
  - Template <i class="fa fa-arrows-h"></i> contrôleur : `[()]`

---

### La feuille de style

___

#### Styles et sous-styles

- Les feuilles de style d'une application Angular respectent la **même notion de construction hiérarchique** que les modules, composants...

- **Le composant racine peut posséder une feuille de style initiale** définissant un style qui sera utilisé pour lui et tous ses sous-composants.

___

#### Explication hiérarchique

- Chaque composant d'un niveau hiérarchique **n** hérite des guides de style de ses composants parents **n-m** pour tout **m>1**. 
- Le style définit à son niveau **n** n'est accessible que pour lui et ses sous-composants **n+m** pour tout **m>=1**. 

___

####  Exemple

```css
button-red {
    background-color: color($colors, red);

    div {
      background-color: yellow;
    }
}
```

___
#### Conclusion

- Un composant hérite du style de ses composants parents
- Sont style ne s'applique qu'au composant et à ses sous-composants
- Peut être écrite en SCSS
  - Utilisation de variables, mixins...

---

## Les services

___

#### <i class="fa fa-book"></i> Définition

D'après Angular, un service est un composant tiers contenant du code métier. Cette définition floue laisse libre au développeur l'interprétation du terme **service**.

___
#### Injection de dépendance

- Angular propose une mécanique d'**injection de dépendance hiérarchique**
- Un **service peut être injecté** grâce à ce système
- On identifiera ainsi un service injectable avec **le décorateur `@Injectable()`**.

___

#### Dépendance d'un service tiers

L'injection de services tiers se fait par le constructeur. Lorsque notre service est instancié par l'application, ses dépendances sont résolues.

___

#### Structure d'un service

```typescript
@Injectable()
export class StoryService {
  constructor(
    public logger: Logger
  ) {}
}
```

- La classe `StoryService` : **injectable**.
- Le constructeurdécrit la dépendance du service **Logger**.
- `public`: facilité syntaxique : = attribut de la classe, accessible par `this.logger`.

___
#### Injection de la dépendance


- Chaque service appartient à un module
	- le module racine d'une application = premier niveau. 

- Chaque module peut inclure plusieurs modules et ce récursivement. 
- L'injecteur cherche à chaque niveau hiérarchique de module si une intance de **Logger** a été instanciée, du niveau courant jusqu'au dernier parent

___

- **Lorsqu'une instance du service a été trouvée** à un étage, **celle-ci est associée à l'attribut** du service instancié, `logger` ici. 
- **Le cas contraire**, **l'injecteur remonte** d'un étage supplémentaire, jusqu'à trouver une instance.

___
### Conclusion

- Un service : potentiellement injectable
- Résolution hiérarchique de dépendance

---

## Les modules

---
#### <i class="fa fa-book"></i> Définition

- **Un module == un ensemble indépendant de fonctionnalités d'une application**. 
- Il **peut contenir** des composants, services, pipes, styles, templates et d'autres modules.
- **Une application Angular == un module**, qui peut inclure d'autres sous-modules.

___ 

<div class="alert alert-info">
<i class="fa fa-check-square-o" aria-hidden="true"></i> Les modules permettent d'organiser une application en la structurant en briques indépendantes facilement réutilisables et testables.
</div>

___

#### Structure d'un module 

- Un module est décrit par le décorateur `@NgModule()`.  
- Plusieurs options de configuration. [Voir la documentation de NgModule](https://angular.io/docs/ts/latest/api/core/index/NgModule-interface.html)

___

#### Exemple de déclaration de module

```typescript
@NgModule({
  declarations: [ // Importation des pages
      MyApp,
      AboutPage,
      ContactPage,
      HomePage
  ],
  imports: [ // Imports d'autres modules
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp], //
  entryComponents: [ // Component factory, est nécessaire en cas d'eager loading
      MyApp,
      AboutPage,
      ContactPage,
      HomePage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
```

___
#### Plusieurs options de configuration
___
`declarations` 

- Déclare** une liste de directives / pipes qui appartiennent au module**
- <span class="text text-warning">**Cela ne les instancie pas !**</span>
___
`providers`  

- **Définit un ensemble d'objets injectables** qui seront utilisables par l'injecteur de ce module
- Cette option est **utilisée par l'injecteur** pour créer les instances de chaque service mentionné et **générer un singleton propre au module**

___
`exports`  

- **Chaque module peut exporter ce qu'il contient** : directives, composants, pipes, services. 
- <span class="text text-warning">**Lorsque ce module sera importé par un module tiers, seuls les éléments exposés ici seront disponibles**</span>
___
`imports`  

- Permet de spécifier une **liste de modules qui seront importés**
- <span class="text text-warning">**Importe les éléments publiques des modules** (directives, composants, pipes, services)</span>
___
`bootstrap`  

- Définit les composants qui devront être amorcés (bootstrappés) lorsque ce module le sera. - Les composants mentionnés ici seront automatiquement ajoutés à `entryComponents`.
___
`entryComponents`  

- Définit une liste de composants qui devront être compilés quand le module est défini. 

---

### La gestion des dépendances

L'injection d'une dépendance de service recherche **l'instance de service cible la plus proche**.

**Cette instanciation peut être réalisée par l'injecteur au niveau du module, d'un composant ou d'un service.**

___

#### Injection dans un module

- l'option `providers` du décorateur `@NgModule()` 
	- **déclare l'instanciation** d'un service 
	- **le rend disponible** pour tous les éléments du modules (services, pipes, composants).

___ 

<div class="alert alert-info">
<i class="fa fa-check-square-o" aria-hidden="true"></i> L'injection dans un module est à favoriser quand un service doit être commun à plusieurs éléments.
</div>
<br>
*Exemple* : mutualisation des données communes entre plusieurs éléments.


___
#### Injection dans un composant / Service

- Déclaration de l'instanciation d'un service dans un composant, service, pipe. 
	- Ces décorateurs possèdent une option `providers`.
- Un service instancié au niveau de ces éléments aura **la priorité la plus importante** lorsque l'injecteur résoudra les dépendances.

___

<div class="alert alert-info">
<i class="fa fa-check-square-o" aria-hidden="true"></i> Ce procédé est à favoriser quand l'instance d'un service doit être unique et spécifique pour un élément.
</div>

___

#### Conclusion

- **Organisation** : Les modules permettent d'organiser structurellement une application Angular
- **Encapsulation** : Chaque modules peut importer plusieurs éléments et rendre public ce qu'il contient 
- **Injection** : La résolution de dépendance est résolue de manière hiérarchique (au plus proche). Chaque élément peut déclarer l'instanciation d'une instance de servie.

---

## Conclusion générale

Cette présentation succinte présente les bases d'Angular. Voir la documentation officielle :

- [Le guide pour démarrer Angular](https://angular.io/docs/ts/latest/guide/)
- [Un tutoriel très bien expliqué](https://angular.io/docs/ts/latest/tutorial/)
- [Des explications avancées concrètes](https://angular.io/docs/ts/latest/guide/animations.html)
- [Un CookBook avec des explications détaillées](https://angular.io/docs/ts/latest/cookbook/)