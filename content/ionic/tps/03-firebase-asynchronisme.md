+++
weight = 10
date = "2018-01-29"
draft = true
title = "3. Firebase et Asynchronisme"
slug = "projet-ionic-async"
tags = ["ionic","service","module"]
categories = [ "ionic" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"            # set "main" to add this content to the main menu
+++

## Firebase

### Création du compte

### Installation

### Intégration dans le module racine

## Requêtes initiales

### Création d'une randonnée

### Récupération de la liste des randonnées

## La liste des randonnées

### Création d'un service `Repository`

### Requête de la liste des randonnées

### Affichage de la liste

## La randonnée

### Requête d'une randonnée

### Affichage de la randonnée sur la page détail

## Statistiques et randonnées

### Stockage en ligne des résultats

## En plus

### Création d'une page historique

### Création d'une page affichant les randonneurs actuels

