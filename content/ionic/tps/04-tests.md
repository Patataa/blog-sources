+++
weight = 10
date = "2018-01-29"
draft = true
title = "4. Tests unitaires et tests e2e"
slug = "tp-ionic-tests"
tags = ["ionic","service","module"]
categories = [ "ionic" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"            # set "main" to add this content to the main menu
+++

## Tests unitaires

### Installation

### Création d'un fichier webpack personnalisé

### Création du fichier d'entrée des tests

### Ecriture du premier test

### Lancement des tests

### Mocker un test unitaire

## Tests e2e

### Installation

### Configuration

### Ecriture du premier test e2e

### Lancement des tests e2e

### Mocker les tests e2e

### Aller plus loin : le patron `page object`

## Documentation

### Installation

### Configuration

### Ecriture de la documentation

### Génération

## Tests et intégration continue Gitlab CI

### Pré-requis

### Création du fichier de lancement

### Configuration de Gitlab

### Lien avec des outils externes

### Tests