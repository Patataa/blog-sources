+++
weight = 10
date = "2018-01-29"
draft = false
title = "4. Modifier le thème de votre applicaton Ionic"
slug = "projet-ionic-theme"
tags = ["ionic","service","module"]
categories = [ "ionic" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"            # set "main" to add this content to the main menu
+++

Dans le cadre de la création d'un thème adapté à votre application, voici un ensemble de ressources pertinentes pour comprendre ces mécanismes.

## Le fonctionnement de l'édition de style des Composants

Au sein d'une application Angular, vous pouvez éditer le style global, l'ensemble de l'apparence. 

En outre, chaque composant peut intégrer sa propre définition de style, isolée du reste du comportement graphique de l'application et sans effets de bords.

[Lien vers la documentation](https://angular.io/guide/component-styles)

## Les thèmes Ionic

Ionic propose une approche particulière pour éditer la charte graphique de l'ensemble de ses composants UI. La documentation est explicite et intègre les bonnes manières pour surcharger les variables de base d'un thème.

[Lien vers la documentation](https://ionicframework.com/docs/theming/basics)


## Ajout d'animations

Angular fournit un guide complet décrivant comment ajouter des animations pour vos composants.

[Lien vers la documentation](https://angular.io/guide/animations)
[Lien vers une démo](http://animationsftw.in)