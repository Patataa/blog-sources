+++
weight = 10
date = "2018-01-29"
draft = false
title = "1. Master-Detail"
slug = "projet-ionic-master-detail"
tags = ["ionic","bases","initialisation"]
categories = [ "ionic" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"            # set "main" to add this content to the main menu
+++

Avant de lire cette première partie, je vous invite fortement à lire de manière détaillée [les attentes du cours](/ionic/cours/attentes-ionic/).

Les TPs présents ici ne représentent pas exhaustivement la charge de travail attendue !

# Création d'une application **Master-Detail**

L'application finale compte 4 pages différentes. Dans le cadre de ce premier TP, vous allez générer les pages, afficher du contenu statique et découvrir les bases d'Ionic et d'Angular.

Il s'agit d'une excellente opportunité de découvrir la documentation des deux outils. 

Ce TP se concentre ainsi sur les pages 2 et 3 : liste des randonnées et affichage de la page détail.

## La page Master

Après avoir générer la racine de votre projet, nous allons générer une première page. 

Ionic possède une fonction CLI dédiée à la génération de contenu : `ionic generate`.

- Générer les pages 2 et 3

Chaque page générée sera la synthèse d'un module, d'un contrôleur et d'une vue.

### Création d'une liste à puce

Sur la page Liste, nous allons ajouter une première liste à puce, il s'agit de la liste des randonnées disponibles.
Au sein de la vue (marquée par le suffixe `.html`), ajoutons une première balise `ul`
Dans un premier temps, le contenu de la liste à puce est ajouté statiquement en HTML.

### Lier la liste à puce avec un tableau JS

Angular permet de lier la vue à des données Javascript.

- Au sein du contrôleur, ajoutez un tableau de string en tant qu'attribut de classe 
- Afficher le contenu de ce tableau tel que chaque élément répètera l'affichage d'une balise `li`au sein de la vue

Pour afficher une variable du contrôleur au sein de la vue, Angular propose l'utilisation d'expression. Ainsi, une primitive est affichable en utilisant : `{{my_attrib}}`.

Pour répéter l'affichage d'une balise, Angular propose la directive `*ngFor`.

### Création d'un type dédiée

Une randonnée ne se résume par une chaîne de caractère. Nous allons donc proposer un modèle adapté à une randonnée.

- Créez une classe modèle Typescript correspondant à une randonnée. Vous définirez les attributs que vous je jugerez utiles.
- Remplacez la collection de string précédemment créée par une collection de randonnées
- Modifiez l'affichage de la randonnée sur la vue

## La page détail

Nous allons faire de même pour la page de détail d'une randonnée.
Dans un premier temps, nous allons gérer l'affichage, puis nous lierons la page de liste avec le détail.

### Ajout d'un attribut Randonnée

Une page de détail affiche le contenu d'une randonnée. Il est ainsi tout naturel que cette page possède un attribut de type Randonnée.

- Ajoutez un attribut randonnée dans le contrôleur de la page détail
- Réalisez un premier affichage des données au sein de la vue

## La liaison entre Master et Detail

Nous avons désormais deux pages : la page liste et la page détail. Cependant, rien de nous permet de naviguer de l'une vers l'autre.
Nous allons donc ajouter le mécanisme de navigation grâce au [composant Ionic dédié](https://ionicframework.com/docs/intro/concepts#navigation).

- Pour utiliser le service de navigation, il est nécessaire de l'injecter dans le contrôleur.
- Ajoutez une fonction dans le contrôleur permettant de naviguer vers la page de détail.
- Liez la fonction du contrôleur avec le clic sur un des éléments de la liste de randonnée.
- Ajoutez un argument à la fonction permettant de transmettre la randonnée ciblée à afficher dans la page détail
- Ajoutons un bouton de retour en arrière dans la page de détail permettant de retourner à la page précédente

## Beautyfication!

Nous avons désormais deux pages liées, permettant de naviguer depuis la liste vers le détail. La prochaine étape est de rendre ça un peu plus joli.

Ionic propose plusieurs composants graphiques respectant la charte *Material Design*.
Nous vous invitons à proposer une implémentation graphique de la maquette dynamique avec ces composants. N'hésitez pas à naviguer dans la documentation et utiliser ce qui vous semble intéressante.

## Le mot de la fin

Pour aller plus loin, il est possible de proposer d'externaliser la représentation d'un objet de la liste de randonnée dans un composant particulier.

Les prochains objectifs sont la découverte des composants natifs et l'utilisateur approfondie des services.