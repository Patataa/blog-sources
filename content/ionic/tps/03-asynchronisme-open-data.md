+++
weight = 10
date = "2018-01-29"
draft = false
title = "3. Asynchronisme et Open Data"
slug = "projet-ionic-async"
tags = ["ionic","service","module"]
categories = [ "ionic" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"            # set "main" to add this content to the main menu
+++

## Data Gouv et Open Data

L'état français met à disposition une plateforme ouverte dédiée à l'OpenData : data.gouv.fr.

Dans le cadre de ce TP, nous pouvez profiter de ces données réelles pour afficher ces randonnées sur vos applications.

La première étape consistera au choix des jeux de données que vous souhaitez intégrer : [C'est par ici !](https://www.data.gouv.fr/fr/search/?q=randonn%C3%A9es)

### Choix du jeu de randonnée

Il existe plusieurs jeux de données avec des formats différents (JSON, CSV...). Vous êtes invités à choisir un jeu contenant suffisant de données pour être intéressant. 

Il est possible de prendre plusieurs jeux des données et d'ajouter des ressources externes, telles que des photographies.

Concernant les étapes, vous n'aurez pas forcément de descriptifs textuels. Vous pouvez considérer les points des lignes comme des étapes à valider.

## Requêtes des randonnées

Vous avez désormais choisi quelles données vous allez afficher. Il va maintenant falloir les requêter.

### Création d'un service dédiée

- Créer un service dédié à la récupération des données
- Injecter le service Angular permettant de réaliser des requêtes HTTP
- Ajouter les fonctions nécessaires pour requête une liste de donnée et une seule donnée (`get(id:string, filter?:any) et getAll() ?`)

### Observable et Adapter

Lors de l'utilisation du composant dédié aux requêtes http Angular, vous allez être confrontés à deux problématiques :

- L'utilisation des types Observables
- La différence des structures de données entre celles requêtées et celles de votre application

La première concerne un paradigme de programmation énormément utilisé en Angular (qui sera requise au partiel final) : la programmation réactive. Pour comprendre comment ça marche, [un peu de lecture](/blog-sources/ionic/cours/promises-vs-observables/), [et un article génial qui fait bien plus que le café](https://gist.github.com/staltz/868e7e9bc2a7b8c1f754).

Concernant la deuxième problématique, il s'agit d'un patron de conception connu : le patron Adapteur. 

### Liaison des services avec les composants

Vos fonctions fonctionnent du tonnerre et renvoient elles-aussi des observables, en appliquant une pipeline de processus de transformation.

Il existe deux manières de récupérer des données asynchrones dans un composant :

- Copier les données dans un tableau / objet qui sera mis à jour lorsque le `subscribe()` d'un observable retournera une valeur
- Directement appeller la fonction asynchrone du service dans le template, en applicant la pipe `async`

Je vous laisse l'opportunité d'essayer les deux démarches, de choisir celle que vous considérer la meilleure.

### Suite des évènements

A partir de maintenant, vous savez faire la différence entre une `promise` et un `observable`.

Vous êtes donc invités à utiliser au maximum ces fonctionnalités dans vos codes sources.
