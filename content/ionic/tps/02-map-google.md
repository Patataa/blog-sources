+++
weight = 10
date = "2018-01-29"
draft = false
title = "2. Composants, services et modules"
slug = "projet-ionic-natif"
tags = ["ionic","service","module"]
categories = [ "ionic" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"            # set "main" to add this content to the main menu
+++

Le but de ce TP est de découvrir l'ajouts de composants tiers et l'utilisation des services.

## Création de de la page activité

Dans un premier temps, nous allons créer la page de randonnée en cours.

### Génération et navigation

- Générer la page 4. Randonnée en cours
- Ajouter la navigation depuis la page détail vers la page Randonnée en cours

Vous veillerez à bien transmettre la Randonnée en tant que paramètre de navigation.

### Ajout des autres composants

Nous allons ensuite ajouter les différents blocs de contenu.

- Ajouter le titre de la page
- Ajouter une `div` vide pour l'emplacement de la map Google
- Ajouter la liste des étapes à réaliser

## Ajout de la map Google

Dans le cadre de ce cours, nous utiliserons le composant Angular-Map (AGM) plutôt que le composant natif Ionic.

Le composant natif n'est accessible que pour Android et IoS, il s'appuie sur l'utilisation native du composant cartographique. Le composant Angular s'appuie quant à lui sur le SDK Javascript.

Je vous invite à suivre le [**getting-started**](https://angular-maps.com/guides/getting-started/) du composant pour son installation. Seule la partie "Setting up Angular Google Maps" nous intéresse ici.

Vous aurez besoin également d'une clé API Google, [que vous pouvez récupérer sur la page dédiée.](https://developers.google.com/maps/documentation/javascript/get-api-key?hl=Fr)

Vous pouvez désormais ajouter le composant au sein de vos pages.

## Géolocalisation

Dans le cadre de notre application, nous ne considérerons qu'un usage actif de la géolocalisation. Cela signifie que nous ne chercherons à mettre à jour l'itinéraire de l'utilisateur que si la page de la randonnée à ouverte.

Nous aurons besoin ici du composant natif Ionic [geolocation](https://ionicframework.com/docs/native/geolocation/).

### Mise à jour du curseur de l'utilisateur

- Ajoutez un curseur sur la map Google
- Récupérez la position de l'utilisateur grâce au composant (attention au cycle de vie de la page !)
- Centrer la carte par rapport à la position de l'utilisateur
- Mettre à jour la position du curseur par rapport à la position de l'utilisateur

### Ajouter les curseurs des étapes

- Pour chaque étape, ajouter un marqueur indiquant sa position.
- Gérer la collision entre l'utilisateur et le périmètre d'une étape

Pour la gestion des collisions, vous pouvez réaliser ça de manière manuelle lorsque la page est active. Vous pouvez également considerer l'utilisation
des [geofence](https://ionicframework.com/docs/native/geofence/).

### Le tracé du parcours

La gestion du tracé du parcours n'est pas documenté dans le composant Angular utilisé.
Cependant, il s'agit de l'encapsulation du SDK JS, une issue [Github existe et propose une solution](https://github.com/SebastianM/angular-google-maps/issues/495).

## Services et modules

Afin de faciliter la lecture du code source, le code métier ne doit pas être présent dans le contrôleur.
Je vous invite ainsi à générer un service Angular dédié à la gestion de la geolocalisation de l'utilisateur et à la détection de la collision avec les étapes à valider.

Le service doit être instancié dans un module adapté, qui doit être importé par le module racine.