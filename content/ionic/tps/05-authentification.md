+++
weight = 10
date = "2018-01-29"
draft = true
title = "5. Authentification"
slug = "tp-ionic-authentification"
tags = ["ionic","service","module"]
categories = [ "ionic" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"            # set "main" to add this content to the main menu
+++

## Le module authentification

### Le service de login avec Firebase

### Aller plus loin : créer un compte / mot de passe oublié

## Les gardiens

### Restreindre l'accès des pages de l'application

### Création d'une page pour l'administrateur : création d'une rando
