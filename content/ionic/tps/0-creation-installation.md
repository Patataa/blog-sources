+++
weight = 10
date = "2018-01-29"
draft = false
title = "0. Installation et initialisation"
slug = "projet-ionic-init"
tags = ["ionic","bases","initialisation"]
categories = [ "ionic" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"            # set "main" to add this content to the main menu
+++

## Installation

La première étape de ce projet est l'installation des dépendances.
Veuillez vous reporter à la section dédiée dans le cours.

### Installation de Ionic et Cordova / Capacitor

La première étape est l'installation de Ionic et Cordova ou Capacitor sur votre machine, en global.
Il est tout à fait possible d'installer les deux dépendances en local à la racine de votre projet.

## Pas d'accès *root* ?

Dans la mesure où vous ne posséder pas les droits en *root* sur votre machine, vous pouvez installer **Ionic CLI** en local et l'utiliser à l'aide de **Npx** [https://www.npmjs.com/package/npx](https://www.npmjs.com/package/npx).
Vous pouvez également utiliser **npm** sans accès *root* [Use npm without root or sudo rights on Linux/](https://topaxi.codes/use-npm-without-root-or-sudo-rights/).

Concernant **Node.JS**, vous pouvez également l'installer en local : [Without root/sudo permission install Node.js on Linux/CentOS](https://www.digizol.com/2017/08/nodejs-install-no-root-sudo-permission-linux-centos.html)

### Création d'un projet de base

La seconde étape est la création du projet de base. Ionic propose des racines de projet fonctionnel.
Dans le cadre de ce TP, nous allons partir du générateur `blank` pour avoir un projet initial fondamental.

Vous êtes fortement invités à découvrir les autres racines de projets. Celles-ci proposent des architectures internes et des outils qui peuvent s'avérer intéressants.

### Installation des paquets

Vous avez le choix des armes, `npm` ou `yarn`.

À la racine du projet nouvellement créé, il suffit de taper

`npm install`

ou

`yarn install`

### Initialisation du repository Git

Le rendu du projet étant à rendre sous format d'un repos git, autant commencer tout de suite ;)

Vous êtes libre d'utilisez le flow qui vous convient.

### Premier lancement web

Les dépendances sont installées, les modules nodes également, les hostilités peuvent commencer.

La commande `ionic serve` ou `npm run serve` à la racine du projet permet d'exécuter l'application. Celle-ci sera accessible directement depuis votre navigateur web.

Par défaut, cette option active un build automatique à chaque modification du code source et un rechargement des pages qui sont ouvertes à la bonne adresse.

### Installation sur mobile

Par défaut, Ionic permet de créer un projet accessible sur navigateur, UNIQUEMENT.

L'intérêt principal de l'outil est de fournir un code exécutable sur plusieurs plateformes. Pour celà, il suffit de les ajouter une par une avec les commandes CLI adéquates. [Voir la documentation Officielle](https://ionicframework.com/docs/cli/cordova/platform/).

Attention cependant ! Tous les plugins natifs ne sont compatibles avec toutes les plateformes ! Il sera nécessaire d'apporter un regard attentif aux pages de documentation de chacune des extensions.
Dans le cadre du travail final, nous attendons une application Android, iOS et PWA.

### Lancement sur mobile

L'exécution sur mobile peut être réalisée par deux moyens différents : l'utilisation des applications **Ionic View** et **Ionic Dev App** ou bien l'envoie de l'application sur le mobile.

Avant de réaliser cette étape, assurez-vous d'avoir bien installer les dépendances liées à chaque plateforme (les sdk adaptés à la plateforme android ciblée par exemple).

Ensuite, il suffit d'exécuter la commande CLI adaptée `ionic cordova run [<platform>]` / `ionic capacitor run [<platform>]`.

- [Voir la page de documentation Cordova Run](https://ionicframework.com/docs/cli/commands/cordova-run/)
- [Voir la page de documentation Capacitor Run](https://ionicframework.com/docs/cli/commands/capacitor-run)

## Le mot de la fin

Voilà, la toute première étape est réalisée. Les choses serieuses peuvent enfin commencer !