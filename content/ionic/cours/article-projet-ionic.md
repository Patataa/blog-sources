+++
weight = 40
date = "2017-04-23"
draft = false
title = "3. Projet : les bases de Ionic"
slug = "projet-ionic-bases"
tags = ["ionic","bases","presentation"]
categories = [ "ionic" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"            # set "main" to add this content to the main menu
+++


En quelques étapes, découvrons comment générer un projet **Ionic**, ajouter des pages et utiliser des composants natifs.

<!--more-->

## Remarque

Les prochaines étapes s'inspirent fortement de la documentation officielle de Ionic.
Il est vivement conseillé de la consulter en cliquant sur le lien suivant : [Documentation de Ionic](https://ionicframework.com/docs/intro/installation/)

## Installation des prérequis

- [Node.js >= 10 et Npm](https://nodejs.org/en/download/)

### Ionic

Ionic propose des commandes CLI. Il est donc nécessaire
d'installer les deux paquets avec la ligne suivante :

```sh
$npm install -g ionic cordova
```

### SDK

Les différentes plateformes requièrent l'installation de kits de développement spécifiques.
Chaque plateforme a son propre kit.

Pour les plateformes **Android** :

- [Android Studio](https://developer.android.com/studio/index.html)
- [Lien vers le manuel d'installation](https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html)

Pour les plateformes **Windows 8.1 et 10** :

- Windows
- [Visual Studio Community](https://www.visualstudio.com/fr/vs/community/)
- SDK Universal Windows Platform Development
- [Lien vers le manuel d'installation](https://cordova.apache.org/docs/en/latest/guide/platforms/win8/index.html)

Pour les plateformes **iOS** :

- XCode...
- [Lien vers le manuel d'installation](https://cordova.apache.org/docs/en/latest/guide/platforms/ios/)

Des solutions Cloud existent.
[Ionic Package](http://blog.ionic.io/build-apps-in-minutes-with-ionic-package/) propose une solution payante permettant de compiler en distant pour les plateformes Android et iOS.
La dernière version de **Ionic AppFlow** propose un utilitaire graphique adapté (version gratuite limitée).

## Générer un projet

Quand les prérequis ont été installés, la génération de projet peut débuter.

Ionic propose une racine de projet de démarrage assez complet par défaut. Plusieurs modèles de projets existent.

Pour télécharger et installer le projet :

```sh
$ionic start myAwesomeProject
```


<div class="alert alert-info">
Ionic propose plusieurs modèles de projets.
</div>

 Pour cela il suffit de taper :

```sh
$ionic start myAwesomeProject <projectType>
```

[Cf. La documentation](https://ionicframework.com/docs/building/starting) :

`<projectType>` :

- `tabs` : Une enveloppe simple avec trois onglets
- `sidemenu` : Une enveloppe avec un menu navigale affichage avec un glissement
- `blank` : une racine vide avec une page blanche
- `super` : un projet complet avec 14 pages intégrées
- `tutorial` : un projet de base, décrit pas à pas

## Exécuter l'application sur une page web

Pour lancer l'application, il suffit de se positionner à la racine du dossier créé et d'utiliser la commande suivante :

```sh
$ionic serve
```

Cette commande met en place un **serveur web**. Ce dernier publie l'application Ionic et la rend **accessible depuis un navigateur internet**.

Si cette option permet de rapidement voir son application sur une page web, elle n'est cependant pas une solution pérenne.  
<div class="alert alert-warning">
<i class="fa fa-warning"></i> Certains plugins natifs ne sont utilisables que sur un terminal mobile. L'application exposée par Ionic serve ne sera donc pas compatible.
</div>

## Exécuter l'application sur un terminal mobile ou un émulateur

Cette étape implique l'installation préalable des kits de développement propre aux plateformes ciblées.

Ionic CLI propose plusieurs commandes permettant de créer une application mobile.

**ionic build**

La première `ionic build ...` compile la brique native, exécute plusieurs traitement sur les ressources web (minification, transpilation...) et crée un projet utilisable sur mobile.

[Lien vers la documentation.](http://ionicframework.com/docs/cli/build/)

**ionic run**

La commande `ionic run <platform> [...options] ` réalise également l'étape de compilation, déploie l'application sur un terminal connecté / émulateur et l'exécute.

Des options de la commandes permettent d'activer une mise à jour automatique de l'application dès que le code source est modifié : `-l`.

[Lien vers la documentation.](http://ionicframework.com/docs/cli/run/)

**Ionic dev app et Ionic View**

Ionic propose deux applications mobiles qui permettent de faciliter le développement et le partage d'applications développés avec leurs outils.

Ionic View est dédié au partage de vos applications en cours de développement avec votre équipe. Il n'est pas nécessaire de déployer l'application sur un store pour être accessible.

Ionic DevApp facilite le développement local. L'application permet de visualiser en temps réel l'état votre projet en cours de développement, sans les désagréments de l'option `livereload`de la commande console.

Afin d'en savoir un peu plus à leur propos, je vous invite à lire l'article suivant et à consulter la document à ce propos.

[Announcing Ionic DevApp](https://blog.ionicframework.com/announcing-ionic-devapp/)

## La structure d'un projet

Voici un descriptif bref de l'architecture de dossier d'une application de base Ionic.

La mention `*` indique que ces dossiers sont gérés automatiquement. Vous n'avez aucune action manuelle à réaliser avec eux.

|_ **hooks** : Les dossiers des hooks cordova, invoqué lors de l'invocation de certaines commandes [cf. doc](https://cordova.apache.org/docs/fr/latest/guide/appdev/hooks/)  
|_ **nodes_modules** : * les paquets npm   
|_ **plugins** : * les plugins cordova installés  
|_ **platforms**: * le code de la partie native compilée, par plateforme  
|_ **resources** : Les différentes ressources natives de l'application, telles que l'îcone ou l'écran de démarrage, variant selon le système d'exploitation
|_ **src** : Le code de l'application  
....|_ **app** : Le module principal de l'application, son point d'entrée  
....|_ **assets** : Les différents ressources statiques  
....|_ **pages** : Un dossier par page, possibilité d'un module par page  
....|_ **theme** : Les fichiers scss du thème + surcharges  
....|_ **index.html** : La page web utilisée, qui exécutera l'application Ionic  
....|_ **manifest.json** : Manifeste de la web app (pour les méta données) [cf. W3C](https://w3c.github.io/manifest/)  
....|_ **service-worker.js** : Code du service worker [cf. MDN](https://developer.mozilla.org/fr/docs/Web/API/Service_Worker_API)  
|_ **www** : * le code de la partie web compilée et minifiée  
|_ **.editorconfig** : Config IDE [cf. website](http://editorconfig.org/)  
|_ **ionic.config.json** : fichier de description / configuration de l'application utilisée par les services cloud notamment  
|_ **package.json** : [configuration d'un projet npm](https://docs.npmjs.com/files/package.json)  
|_ **tsconfig** : options de [transpilation des fichiers typescript](https://www.typescriptlang.org/docs/handbook/tsconfig-json.html)  
|_ **tslint** : configuration du [linter typescript](https://palantir.github.io/tslint/)  

***./src/index.html***

Le fichier `./src/index.html` est donc la page d'entrée de l'application. Cette page charge l'ensemble des scripts, inclue les CSS et exécute l'application.

Comme les dépendances avec les bibliothèques et feuilles des style tierces sont gérées par compilation, nous aurons peu de modification à réaliser sur cette page.

***.src/app/app.module.ts***

Le fichier `.src/app/app.module.ts` est le point d'entrée de l'application.
Il s'agit du fichier déclarant le module principal "racine".

```typescript
// Import des différentes dépendances (pour la compilation Typescript)
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// NgModule est un décorateur : il génère le code pour déclarer un module Angular
@NgModule({
  // Déclare l'existence des différents modules : ici chaque page est déclarée.
  // Une déclaration ne correspond pas à une instanciation. Il s'agit juste de préciser
  // que ces composants seront utilisés par le module et ses sous-modules
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  // *imports* permet d'injecter un module tiers. Le module courant pourra ainsi
  // utiliser les membres publiques des modules importés
  imports: [
    BrowserModule, // Nécessaire pour exécuter l'application dans un navigateur
    IonicModule.forRoot(MyApp) // Importe et construit une application Ionic
  ],
  // Instancie et exécute l'application Ionic
  bootstrap: [IonicApp],
  // Instancie les différentes pages une par une.
  // Cette précision n'est plus obligatoire depuis Ionic 3 : les pages peuvent
  // être chargée dynamiquement par lazy loading
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  // Inject les différents services au niveau du module racine.
  // Ces services seront commun à l'ensemble des sous-modules de l'application
  providers: [
    StatusBar,
    SplashScreen,
    // La ligne suivante permet de spécifier un type différent de gestionnaire d'erreur
    // IonicErrorHandler implémente l'interface ErrorHandler
    // Il s'agit d'un bon exemple de l'intérêt du système d'injection de dépendance
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
// Le module racine est rendu publique pour être utilisée en extérieur
export class AppModule {}
```

1. Le module racine va donc charger les dépendances globales nécessaires à l'exécution de l'application (composants et services).  
2. Il va également charger le composant de base, ici `MyApp` qui sera le composant initial qui encapsulera tous les autres.  
3. Puis l'application sera instanciée et exécutée (bootstrap).


***.src/app/app.component.ts***

Comme décrit à la section précédente, ce composant est le composant initial qui encapsulera tous les autres.

Son rôle par défaut est simple : 

1. Il définit la page de base à utiliser pour la navigation
2. Quand l'application est prête, il retire le splash screen (écran chargement)
3. L'application est "redirigée" vers la page racine.

Dans le cas du projet "tabs", il s'agit de la page `TabsPage` qui propose une navigation selon des onglets.

## Ajouter une page

**Ionic** propose un générateur de page CLI. [Voir la documentation](https://ionicframework.com/docs/cli/generate/).

L'utilisation de la commande permet de générer le composant : typescript, html et css.

```sh
# ionic g page <PageName>
$ionic g page myPage

√ Create app/pages/my-page/my-page.html
√ Create app/pages/my-page/my-page.ts
√ Create app/pages/my-page/my-page.scss
```
Cf. Documentation officielle.

<div class="alert alert-info">
    La commande ne permet que de générer les fichiers par défaut. Il est ensuite nécessaire de configurer la navigation.
</div>

## Naviguer entre les pages


### Naviguer en ***typescript*** avec `NavController`

[Lien vers la documentation](https://ionicframework.com/docs/api/navigation/NavController/)

Cette section considère que vous ayez déjà initialisé un contrôleur de navigation (réalisé par défaut lors de la création d'un projet starter).

**Injection de `NavController`**

En injectant le service `NavController`, vous récupèrerez l'instance la plus proche hierarchiquement du composant courant. Cela signifie que si vous ne l'avez pas redéfini dans les `providers` d'un composant / module parent, vous récupèrerez l'instance du module racine.

***Navigation avec Ionic 4+**

Pour les versions 4+ utilisant Angular (notre cas), il est fortement conseillé d'employer la navigation Angular. Je vous invite à lire [la section dédiée de la documentation officielle](https://angular.io/guide/router).

***Navigation avec Ionic[2, 3]**
Attention, ce mode de navigation ne concerne que les versions de Ionic antérieures à 4 ! 

[La documentation de Ionic concernant `NavController` est assez explicite.](https://ionicframework.com/docs/api/navigation/NavController/)

Elle explique suffisamment clairement comment aller d'une page à une autre.
La documentation décrit également la notion de cycle de vie d'une page : les évènements qui se déroulent lors de l'ouverture / fermeture.

### Naviguer en ***HTML*** avec `NavPush / NavPop`

Il est également possible de naviguer vers une page particulière en utilisant les directives `NavPush / NavPop` directement sur un lien ou un bouton.

```html
<button ion-button [navPush]="pushPage" [navParams]="params">Go</button>
```

L'identifiant **pushPage** est associé à une instance de page tel que :

```typescript
import { LoginPage } from './login';

@Component({
  template: `<button ion-button [navPush]="pushPage" [navParams]="params">Go</button>`
})
class MyPage {
  public pushPage: any;
  public params: any;
  constructor(){
    this.pushPage = LoginPage;
    this.params = { id: 42 };
  }
}
```

[Voir la documentation](https://ionicframework.com/docs/api/components/nav/NavPush/)

### Utiliser des paramètres lors d'un changement de page

**Spécifier des paramètres**

Lors d'une navigation vers une page, il est possible de spéficier des paramètres `NavParams`.
[Voir la documentation](https://ionicframework.com/docs/api/navigation/NavParams/).

Par exemple, les lignes suivantes permettent de naviguer vers la page `login` en ajoutant des paramètres utilisateurs :

```typescript
export class StartPage{
  constructor(public navCtrl: NavController) {
  }

  goToLogin(){
    this.navCtrl.push(LoginPage, {
      userParams: "1"
    });
  }
}

```

Avec la directive `navPush`, il existe également la directive `navParams`. 

```html
<button ion-button [navPush]="loginPage" [navParams]="params">Go</button>
```

Dans l'exemple ci-dessus, l'identifant `params` est associé à l'**Object** suivant : 

```javascript
public params = {
  userParams: "1"
};
```

**Récupérer des paramètres**

Chaque paramètre est associé à un identifiant. La fonction `get` permet de récupérer la valeur associé à l'identifiant passé en paramètre.

Ils sont récupérables de la manière suivante :

```typescript
export class LoginPage{
 constructor(public params: NavParams){
   // userParams is an object we have in our nav-parameters
   this.params.get('userParams');
 }
}
```


## Utiliser un composant graphique

On distingue deux types de composants graphiques :

- Ceux nécessitant un service pour générer un contrôleur
- Les autres

Les premiers types impliqueront l'injection d'une dépendance dans le composant les utilisant.
Les deuxièmes types sont injectés par défaut dans le module de l'application.

[La documentation est assez clair et détaillée.](http://ionicframework.com/docs/components/)

**Ionic** propose plusieurs dizaines de composants différents s'appuyant sur le thème graphique [**Material design**](https://material.io/guidelines/material-design/introduction.html).

## Utiliser un composant natif

> Un composant natif désigne un composant ayant des intéractions directes avec le système d'exploitation du terminal mobile : gestion des fichiers, caméra, micro...


### Prérequis

L'utilisation de composants natifs implique la présence du packet `@ionic-native/core`, installable de la manière suivante :

```sh
$npm install @ionic-native/core --save
```
<div class="alert alert-info">
Par défaut, ce paquet est présent lors de la création d'un projet Ionic.
</div>

### Installation d'un plugin natif

L'installation d'un plugin natif se déroule en deux étapes.

.1 Télécharger le paquet `npm`

```sh
$npm install @ionic-native/camera --save
```

.2 Installer le plugin avec **Ionic** ou **Cordova** 

```sh
$ionic plugin add cordova-plugin-camera
```


<div class="alert alert-warning">
    <i class="fa fa-warning"></i>
Certains plugins requièrent des étapes d'installations supplémentaires. Voir leur documentation respective.
</div>

### Utilisation du plugin

Quand le plugin a été téléchargé et installé, il est utilisable en tant que service Angular.
Vous pouvez désormais l'injecter dans le module dont vous en aurez besoin.

*L'exemple suivant injecte le service* ***Camera*** *dans le module principal de l'application*

```sh
...

import { Camera } from '@ionic-native/camera';

...

@NgModule({
  ...

  providers: [
    ...
    Camera
    ...
  ]
  ...
})
export class AppModule { }
```

<div class="alert alert-warning">
    <i class="fa fa-warning"></i>
    La documentation des composants natifs n'est pas forcément exhaustive. Il vous sera souvent nécessaire de lire également la documentation du plugin Cordova lié (accessible sur la page de documentation de chaque plugin natif).
</div>


### Sauvegarde de l'état de l'application

En suivant les deux étapes précédentes, vous avez installer le paquet **npm** et le plugin **Cordova**.

Il est possible que votre projet n'ait pas inscrit ces plugins dans le fichier `package.json`. Cela signifie que si vous partagez votre projet à un de vos collègues, il devra réitérer les opérations d'installation **manuellement**.

Pour sauvegarder l'état de l'application, il suffit d'utiliser la commande suivante :

```sh
$ionic state save
```

<div class="alert alert-warning">
  <i class="fa fa-warning"></i> Cette commande est dépréciée. Favorisez l'utilisation de <strong>ionic platform</strong>.
</div>

**Ionic** proposant une surcouche à **Cordova**, la documentation relative à cette commande est [celle de Cordova](https://cordova.apache.org/docs/en/latest/platform_plugin_versioning_ref/).


### Conclusion

Vous avez désormais les bases pour débuter avec **Ionic**.

N'hésitez pas à consulter [la documentation officielle](https://ionicframework.com/docs/) pour avoir des précisions plus fines concernant le fonctionnement de l'outil.

### Références

- [**Documentation officielle de Ionic**, Ionic, 2017](https://ionicframework.com/docs/)
- [**Ionic 2 Cookbook second edition**, Hoc Phan, 2016](https://www.packtpub.com/web-development/ionic-2-cookbook-second-edition) 