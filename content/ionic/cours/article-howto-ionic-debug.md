+++
weight = 60
date = "2017-04-23"
draft = false
title = "4. HowTo: Débugger un projet Ionic avec Chrome"
slug = "howto-ionic-debug"
tags = ["ionic","debug","tuto"]
categories = [ "ionic" ]
image = "/images/howto-ionic-debug/cover.jpg"
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"             # set "main" to add this content to the main menu
+++


Et si on remplaçait la méthode artisanale de correction de bug s'appuyant **massivement** sur `console.log` par une manière plus intelligente ?

Avec la méthode suivante, vous pourrez en toute simplicité parcourir le code source de votre application, ajouter des **breakpoints**, observer le contenu des variables...

Découvrons simplement comment débugger un code **Ionic** typescript s'exécutant en distant sur votre mobile, depuis le navigateur de votre ordinateur.

<!--more-->

## Prérequis

Nous partons du postulat que vous avez déjà généré un projet **Ionic**, qu'il s'exécute en local et que vous savez comment faire pour l'exécuter sur un téléphone mobile. Le cas contraire, je vous invite à parcourir la page suivante : [/post/projet-ionic-bases/](Les Bases de Ionic).

Pour réaliser l'opération de débug, nous allons utiliser le navigateur **Chrome**.

Eléments nécessaires : 

- Un projet Ionic s'exécutant sur le téléphone (`$ionic run --device` par exemple)
- Activation des options de debug développeur sur votre téléphone
- Google Chrome

<!--
## Mettre en place les *source-maps*

### C'est quoi une *source-map* ?

**Ionic** s'appuie sur **Angular** qui lui même utilise **Typescript**. Cependant, le typescript ne s'exécute pas dans un navigateur. Il est nécessaire de le transpiler pour le transformer en Javascript.

Sauf que voilà, débugger un code js transpilé s'avère une tâche inefficace, longue et douloureuse.

Nous allons donc autoriser notre compilateur Typescript <i class="fa fa-long-arrow-right"></i> Javascript à générer des fichiers de correspondances Typescript / Javascript.

Ces fichiers de correspondances s'appellent des **source-maps**. Nous allons ainsi indiquer au navigateur que le code JS qu'il utilise correspond à un code TS particulier. En gros, cela va nous permettre d'afficher le code source Typescript dans l'outil de debug de chrome.

Prêt ?

### Surcharger la configuration Webpack
-->

## Configurer votre périphérique Android

Sur votre périphérique Android, allez dans `paramètres/Options développeurs/` et assurez vous que l'option `Débogage USB` soit activé.

La catégorie `Options développeurs` est cachée par défaut. Si vous ne l'avez pas activée, il suffit de réaliser la démarche suivante :

1. Ouvrir la page des paramètres
2. Cliquer sur la section `À propos du téléphone`
3. Appuyer 7 fois sur la section `Numéro de build`
4. Retourner à la section précédente, l'option devrait être visible.

[cf. La Documentation officielle](https://developer.android.com/studio/debug/dev-options.html) 

## Utiliser le débuggeur de **Chrome**

Une application Ionic s'exécute dans une Webview. Cela signifie que nous pouvons utiliser les outils dédiés au debug d'applications web, même distantes !

La première étape consiste à exécuter votre application Ionic (sur android ici) sur un téléphone connecté à votre ordinateur :

    $ ionic run android --device

Dès que l'application est chargée, nous pouvons exécuter Chrome depuis notre machine de développement.

<div class="alert alert-warning">
    Pour utiliser les outils de développement, Chrome requiert une authentification avec un compte Google. Le debug distant ne fonctionne pas en mode Inconnu ou en mode invité !
</div>

Depuis le navigateur, appuyez sur la touche `F12`. Cela devrait ouvrir les outils de développeur :

![Console de Google Chrome](/images/howto-ionic-debug/debug-console-chrome.jpg)
<center>Console de Google Chrome</center>

Puis, cliquez sur le bouton tout à droite avec l'îcone 'trois points' à côté de la croix. Puis : `More tools / Remote Devices`. La liste des périphériques connectés devrait s'afficher.

![Onglet des périphériques connectés et des éléments débuggables](/images/howto-ionic-debug/remote-devices-chrome.jpg)
<center>Onglet des périphériques connectés et des éléments débuggables</center>

Si un navigateur est ouvert sur votre téléphone Android, les différents pages actives devraient également être présentes.

Dans notre cas, seul notre application nous intéresse. Il suffit de cliquer sur le bouton `inspect` à droite de l'application **Ionic App**.

Vous obtiendrez la fenêtre suivante : 

![Aperçu de la console de debug](/images/howto-ionic-debug/debug-preview-chrome.jpg)
<center>Aperçu de la console de debug</center>

## C'est parti pour les BreakPoints !

L'outil de développement vous permet d'intéragir avec l'application directement depuis votre ordinateur.
Depuis l'onglet des sources, vous aurez accès au code du logiciel. 

Vous pourrez ainsi à loisir ajouter des **breakpoints**, analyser le contenu des variables, afficher le contenu de la console, consulter les intéractions réseaux... Bref, un ensemble de fonctionnalités pratiques pour le debug vous est maintenant disponible.

![Debug assisté par breakpoint](/images/howto-ionic-debug/debug-breakpoint-chrome.jpg)
<center>Debug assisté par breakpoint</center>

## Au secours ! Mes sources ne s'affichent qu'en Javascript !

Il est possible que votre version d' `ionic-app-script` soient buggées ([voir l'issue liée](https://github.com/driftyco/ionic-app-scripts/issues/822)).

<div class="alert alert-danger">
<i class="fa fa-warning"></i><strong> Pour le moment, la solution évoquée ne permet pas de compiler en production !</strong>
Cependant, cela vous permettra de débugger votre application sereinement en phase de développement.
</div>

La démarche à réaliser est simple, il suffit d'ajouter les lignes suivantes au fichier `package.json` principal de votre application :

```json
    {
        //...
        "config": {
            "ionic_bundler": "webpack",
            "ionic_source_map_type": "#inline-source-map"
        },
        //...
    }
```

## Conclusion

L'utilisation des outils de debug distant facilite grandement la tâche de développement.

Son utilisation requiert la configuration du téléphone et un accès depuis votre navigateur chrome. Ensuite, vous pourrez utiliser l'outil comme si vous débuggiez une page web traditionnelle.

## Références

- [**Utiliser une source map**, Mozilla Developer Network](https://developer.mozilla.org/fr/docs/Outils/D%C3%A9bogueur/Comment/Utiliser_une_source_map)
- [**Get Started with Remote Debugging Android Devices**](https://developers.google.com/chrome-developer-tools/docs/remote-debugging)