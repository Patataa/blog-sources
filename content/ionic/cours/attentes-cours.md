+++
weight = 15
date = "2017-04-19"
draft = false
title = "0.1 Attentes du cours"
slug = "attentes-ionic"
tags = ["attentes"]
categories = [ "ionic" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = "ionic"          # set "main" to add this content to the main menu
+++

Les attentes du cours, livrables demandés et mode d'évaluation.

<!--more-->

## Projet Ionic

<i class="fa fa-clock-o"></i> <strong>Durée</strong> : 14 heures de TP  
<i class="fa fa-user"></i>**Binôme de projet**  
<i class="fa fa-book"></i> **Sujet** : Création d'une application mobile

En 7 séances de TP, vous serez ammenés à développer une application mobile Ionic.

Le sujet de l'application mobile est **Rando Auvergne**. Vous trouverez ci-dessous les spécifications fonctionnelles du résultat attendu.

### Contraintes 

Les points suivants sont considérés comme **impératifs**. Toute absence de ces éléments entraînera le retrait de points.

- Tests unitaires
- Tests fonctionnels
- Une compatibilité avec deux plateformes différentes :
    - Android
    - iOS
    - Windows
    - PWA
    - ... (Voir les plateformes Cordova)
- Documentation : le code doit être documenté
- Découpage modulaire
- Une page de rapport : `README.MD` indiquant les points suivants
    - Comment installer votre application pour le développement
    - Ce que vous avez réalisé (des phrases courtes)
    - Ce que vous n'avez pas réalisé et pourquoi
    - Les éléments supplémentaires ajoutés au projet

Les points suivants sont considérés comme **conseillés**. Leur présence ajoutera des points supplémentaires (bonus).

- Rapports de tests (de type Istanbul)
- Utilisation d'environnements de build (Docker, VM...)
- Intégration continue (Gitlab CI, Jenkins...)

### Spécifications fonctionnelles

Le but final de ce travail pratique est la réalisation d'une application de randonnée.
Le résultat attendu comporte quatre écrans : 

1. Un écran d'authentification
2. Une liste des différentes randonnées accessibles
3. Le détail d'une randonnée
4. La randonnée en cours

#### Maquette

Vous trouverez à l'adresse suivante la maquette dynamique de l'application attendue : 

[Lien vers la maquette](https://app.moqups.com/chariere.fiedler.cedric@gmail.com/MsvkuG0EKn/view?ui=0)

#### Descriptif des pages

**[1.] Facultatif : Page d'authentification**

**[1.a] Feature**:  Page Connexion - Authentification

**[1.a.1] Scénario**: L'utilisateur se connecte avec de bons identifiants  
**Soit** Bob utilisant **Bob** en tant que login et **toto** en tant que mot de passe   
**Quand** il clique sur le bouton connexion  
**Alors** il est redirigé sur la page listant les randonnées [2.]

**[1.a.2] Scénario**: L'utilisateur se connecte avec de mauvais identifiants  
**Soit** Bob utilisant **Bob** en tant que login et autre chose que **toto** en tant que mot de passe   
**Quand** il clique sur le bouton connexion  
**Alors** un message d'erreur s'affiche indiquant qu'il ne peut pas se connecter

---

**[2.] Page Liste des randonnées**

**[2.a] Feature**: Affichage des randonnées

**[2.a.1] Scénario**: Affichage des randonnées en ligne  
**Soit** L'utilisateur possède une connexion internet  
**Quand** Il arrive sur la page  
**Alors** Les randonnées disponibles sur le serveur sont affichées sur la page

**[2.a.2] Scénario**: Affichage des randonnées hors-ligne  
**Soit** L'utilisateur ne possède pas de connexion internet  
**Quand** Il arrive sur la page  
**Alors** Un message d'erreur s'affiche indiquant qu'il est impossible de se connecter au serveur  

Ce scénario est facultatif  
**[2.a.3] Scénario (Optionnel)**: Affichage des randonnées hors-ligne  
**Soit** L'utilisateur ne possède pas de connexion internet  
**Quand** Il arrive sur la page  
**Alors** Les dernières randonnées téléchargées lorsque l'utilisateur avait internet sont affichées  

-

**[2.b] Feature**: Navigation vers la page détail  

**[2.b] Scénario**: Navigation vers la page détail  
**Soient** Les randonnées sont affichées sur la page  
**Quand** L'utilisateur clique sur une des randonnées  
**Alors** L'utilisateur est redirigé sur la page détaillant une randonnées [3.]  

-

**[2.c] Feature**: Navigation vers la page détail  

**[2.c] Scénario**: Affichage des données de la randonnée en cours  
**Soit** L'utilisateur a une randonnée en cours    
**Alors** Le temps depuis le début de la randonnée est affichée en bas de la page    
**Quand** L'utilisateur clique sur le cadre du bas de page indiquant le temps  
**Alors** Il est redirigé vers la page indiquant la randonnée en cours [4.]  

---

**[3.] Page**: Détail d'une randonnée  

**[3.a] Feature**: Affichage des randonnées  

**[3.a.1] Scénario**: Affichage des randonnées    
**Soit** L'utilisateur arrive sur la page Détail d'une randonnée [3.]  
**Alors** Les informations relatives à la randonnées téléchargées depuis le serveur sont affichées sur la page  

-

**[3.b] Feature**:  Démarrer la randonnée

**[3.b.1] Scénario**: Démarrage de la randonnée  
**Quand** L'utilisateur clique sur l'un des boutons **start**  
**Alors** Il est redigiré vers la page de la randonnées en cours [4.]  
**Et Alors** La randonnée courante est considérée comme la randonnée active  

---

**[4.] Page**: Randonnée en cours

**[4.a] Feature**: Démarrage de la randonnée

**[4.a.1] Scénario:** Démarrage du chronomètre  
**Soit** L'utilisateur a cliqué sur un des boutons starts de la page [3.] (ref. Scénario 3.b.1)  
**Quand** L'utilisateur arrive sur la page Randonnée en cours [4.]  
**Alors** Le chronomètre est initialisé à 0  
**Et Alors** Le chronomètre démarre  

**[4.a.2] Scénario:** Démarrage de la géolocalisation    
**Soit** L'utilisateur est sur la page [4.]  
**Quand** Toutes les 5 secondes  
**Alors**  la position de l'utilisateur est rafraîchie    
**Et Alors** Le curseur de position est déplacé sur la carte    
 
 -

**[4.b] Feature**: Validation d'une étape  

**[4.b.1] Scénario:** L'utilisateur valide une étape  
**Soit** L'utilisateur est en mouvement  
**Quand** La position de l'utilisateur rencontre une zone de validation de l'étape (un geofence)    
**Alors** L'étape courant devient la précédente et la suivante devient la courante  
**Et alors** La liste des étapes scrolle automatiquement vers l'étape courant  

-

**[4.b] Feature**: Fin de la randonnée 

**[4.b.1] Scénario:** L'utilisateur valide la dernière étape  
**Soit** L'utilisateur est en mouvement   
**Quand** Il valide la dernière étape  
**Alors** Le chronomètre s'arrête  

-

**[4.b.1] Scénario:** L'utilisateur termine manuellement la randonnée  
**Soit** L'utilisateur est en mouvement   
**Quand** Il clique sur le bouton **Finish**  
**Alors** Le chronomètre s'arrête