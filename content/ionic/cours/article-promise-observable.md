+++
weight = 80
date = "2017-04-23"
draft = false
title = "6. Promises VS Observables"
slug = "promises-vs-observables"
tags = ["angular","promise","observable"]
categories = [ "ionic" ]
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"             # set "main" to add this content to the main menu
+++
Il existe en JS des fonctions dites synchrones et asynchrones. Les fonctions synchrones s'exécutent séquentiellement. Tandis que l'invocation de l'exécution de fonctions asynchrones ne garantie pas leur ordre d'exécution.

Il existe plusieurs moyens pour garantir l'ordre d'exécution d'une fonction asynchrone. Nous aboderderons ici les notions de Callback, de Promise et d'Observable en vous indiquant des lectures qui vous expliciterons les notions.

<!--more-->

## Async VS Sync
<!--
- JS: Langage monothread
- ? => traitement non bloquant = évènements
- Event long à écrire : interface simplifiée : promises
- Fonction synchrone : retourne un résultat
- Fonction asynchrone : retourne un intermédiaire
- Chaînage possible des intermédiaire


Le Javascript est un langage interprété mono-thread. Cela signifie que tout traitement réalisé est séquentiel et bloquant.
Cependant, 
Les mécanismes d'asynchronismes sont introduits par les évènements Javascript.

L'asynchronisme définit le caractère non bloquant d'une opération. Un programme appelant une tâche asynchrone A, invoque son exécution. Il n'attend pas son retour pour initier l'invocation du code suivant. 
-->

<!--
On qualifie de synchrone, tout programme s'exécutant dans la continuité de son invocation. Tant que ce programme ne s'est pas terminé, il est bloque l'exécution d'autres programmes.

Ce comportement s'avère problématique dans le cadre de la réalisation d'interfaces utilisateurs. Imaginons que nous récupèrerions des données depuis un webservice de manière synchrone. Cela signifirait que l'interface serait inutilisable tant que la récupération de données n'a pas abouti.

Javascript est un langage mono-thread. Dans un programme mono-thread, tout traitement réalisé est séquentiel et bloquant.
Cependant, le JS utilise un mécanisme particulier qu'on appelle **la boucle évènementielle**. L'objectif de l'article n'était pas de le détailler, nous vous invitons à parcourir le lien pour voir ce qu'il en est : [**Gestion de la concurrence et boucle des événements**, Mozilla Developer Network](https://developer.mozilla.org/fr/docs/Web/JavaScript/Concurrence_et_boucle_des_%C3%A9v%C3%A9nements).

Le JS autorise ainsi le mécanisme d'asynchronisation par le biais de cette boucle. Cela indit 
-->

Il existe plusieurs moyens pour garantir l'ordre d'exécution d'une fonction asynchrone. Le premier est de mettre en place une fonction de callback directement dans cette fonction.

```typescript

function asyncFoo(i, myCallback) {

    setTimeout(()=> {
        // your stuff
        ...
        myCallback(i+1);
    }, 3000);
}
```

Le chaînage multiple de fonctions asynchrones à base de callback entraîne des pyramides horizontales illisibles :

```typescript
asyncFoo1(1, (i)=> { 
    asyncFoo2(i, (j)=>{
        asyncFoo3(j, (k)=> {
            asyncFoo4(k, (l))=> {
                console.log("Hello World!", l);
            }
        })
    }
});

```

C'est pourquoi des mécanismes comme les *promises* et les *observables* ont été créés.

## Les Promises

Les *promises* permettent de remplacer la pyramide de callback vue plus haut par la syntaxe suivante :

```typescript
asyncFoo1()
    .then(asyncFoo2)
    .then(asyncFoo3)
    .then(asyncFoo4)
    .then(()=> {
        console.log("Hello World !");
    })
```

Les *promises* sont des objets utilisés pour réaliser des traitements asynchrones. Chaque promesse retourne un objet intermédiaire qui pourra retourner une valeur plus tard, ou pas du tout.

Les *promises* sont particulièrement utiles pour les fonctions asynchrones qui réalisent un traitement unique et ne retourne qu'une ou aucune valeur.

Une promise se créer de la manière suivante :

```typescript
let p = new Promise<string>((resolve, reject)=> {
    let result = confirm("Press a button!");
    if (result === true) {
        resolve();
    } else {
        reject();
    }
})

```

Le constructeur d'une promise prend en paramètre un exécuteur : une fonction à laquelle on passera deux arguments, resolve et reject. Resolve résoud la promise, tout paramètre qui lui est passé `resolve(myArg)` sera chaîné à la prochaine promise par `.then(myArg)`. Reject retourne une erreur, tout paramètre passé à resolve, sera passé à `.catch()`.

N'hésitez pas à regarder [la référence JS proposée par Mozilla Developer Network](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Promise) pour comprendre en profondeur son fonctionnement.

## Les Observables

Angular s'appuie en grande partie sur les Observables RxJS. Il s'agit d'un patron de conception issue sur une variante du patron *Observer*.

A la différence des Promises qui sont des actions asynchrones ne retournant qu'une seule valeur, les Observables sont des *streams* qui vont émettre plusieurs valeurs jusqu'à leur clôture. Le développeur va ainsi connecter / inscrire des fonctions qui vont observer les valeurs émises par les streams. 

Je vous invite à lire l'excellente [Introduction à la programmation réactive](https://gist.github.com/staltz/868e7e9bc2a7b8c1f754) proposée par Andre Staltz. Cet article vous expliquera simplement les notions de ce paradigme de programmation.

Et à parcourir [le site officiel de ReactiveX](http://reactivex.io/) (Angular s'appuie sur son implémentation JS).

<!--
## Typescript : Async / Await

- Sucre syntaxique
- Exemple de code
- Prérequis de la fonction
    - mot clé async pour celle qui await + retour valeurs
    - 
-->

## Références

- [**Gestion de la concurrence et boucle des événements**, Mozilla Developer Network](https://developer.mozilla.org/fr/docs/Web/JavaScript/Concurrence_et_boucle_des_%C3%A9v%C3%A9nements)
- [**Doc Http**, Angular](https://angular.io/docs/ts/latest/tutorial/toh-pt6.html#!#observables)
- [**Promise**, Mozilla Developer Network](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Promise)
- [**RxJS Observable**, novembre 2016, Ippon](http://blog.ippon.fr/2016/11/16/rxjs/)

- [**The introduction to Reactive Programming you've been missing**, Andre Staltz]((https://gist.github.com/staltz/868e7e9bc2a7b8c1f754))