+++
weight = 70
date = "2017-05-22"
draft = false
title = "5. Réaliser des tests fonctionnels avec Ionic, Protractor et Appium"
slug = "howto-ionic-e2e-protractor-appium"
tags = ["ionic","e2e","tuto", "protractor", "appium"]
categories = [ "ionic" ]
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"           # set "main" to add this content to the main menu
+++

**Les tests fonctionnels** (end 2 end) sont dédiés à la **vérification du bon fonctionnement d'un système informatique**, d'un point de vue utilisateur.

Le logiciel est considéré dans son ensemble : front, back, api extérieures...

L'objectif est de **tester l'application comme si nous étions des utilisateurs**, en rédigeant des scénarios correspondant aux spécifications fonctionnelles du cahier des charges.

Nous détaillerons dans ce poste comment mettre en place **une suite utilitaire dédiée à l'exécution automatique de tests fonctionnels** pour une application Ionic, avec Protractor et comment **les exécuter directement sur un périphérique Android** avec Appium.

<!--more-->


## Prérequis

Pour les besoins de l'explication, nous avons besoin des éléments suivants :

- Un projet Ionic fonctionnel
- Node et npm
- Un périphérique Android connecté à notre machine de développement, avec les options de développeur activées

## Installation des outils

Installation globale des outils : 

    $ npm install -g protractor appium wd wd-bridge

- [Appium](http://appium.io/) : Surcouche Selenium pour l'automatisation pour les applications mobiles
- [Protractor](http://www.protractortest.org/#/) : Framework de test pour les applications Angular et AngularJS
- [wd](http://admc.io/wd/) : Un client node pour le webdriver/selenium 2
- [wd-bridge](https://github.com/sebv/wd-bridge) : Bridge entre WD et les autres clients selenium

### Protractor

**Protractor** est une surcouche à [WebdriverJS](https://github.com/SeleniumHQ/selenium/wiki/WebDriverJs) (Selenium) permettant d'exécuter des tests dans un navigateur web. Cette surcouche fournit une interface pour faciliter les intéractions avec l'application web. Protractor supporte nativement les fonctionnalités Angular, ne nécessitant pas d'effort de configuration supplémentaire.

Protractor n'est pas dédié à l'écriture des tests, mais à leur exécution. Il utilise par défaut le framework [Jasmine](https://jasmine.github.io/), surchargeable (comme [Mocha](https://mochajs.org/)).

- [Protractor](http://www.protractortest.org/#/)

### Appium

**Appium** fournit une couche d'abstraction permettant d'automatiser les intéractions avec des applications mobiles iOS, Android et Windows. La solution s'appuie sur une structure Client/Serveur exposant une API REST. Le serveur est connecté aux émulateurs / périphériques et réalise les scénarios transmis par les clients.

Dans notre cas, son utilisation est abstraite par Protractor.

- [**Introduction to Applium**, appium](http://appium.io/introduction.html)


## Configuration de Protractor et d'Appium

Les outils sont installés sur la machine de test. Nous allons donc configurer Protractor afin de lui spécifier l'adresse du serveur Appium, l'emplacement des scénarios de test fonctionnel, le périphérique à utiliser, la position de l'application...

Voici un exemple de fichier de configuration `protractor.conf.js`:

```js
//protractor.conf.js
let path = require('path');
let wd = require('wd');
let protractor = require('protractor');
let wdBridge = require('wd-bridge')(protractor, wd);

exports.config = {
    // Adresse du serveur Appium par défaut 
    seleniumAddress: 'http://localhost:4723/wd/hub',
    // Emplacement de nos tests fonctionnels. Nous chargeons ici tous les fichiers présents dans le dossier ./e2e
    // et suffixés par .e2e.js
    specs: ['./e2e/**/*.e2e.js'],
    // Configuration du mobile sur lequel sera exécuté les tests. 
    // Pour ajouter plusieurs cibles, voir : https://github.com/angular/protractor/blob/master/lib/config.ts
    capabilities:
    {
        // Le nom de la plateform : android | ios | windows
        platformName: 'android',
        deviceName: 'moto X play',
        browserName: "",
        autoWebview: true,
        // Nous indiquons ici l'emplacement vers l'application à tester, artefact de la commande `ionic build android`
        app: path.join(__dirname, '/platforms/android/build/outputs/apk/android-debug.apk'),
        // appWaitDuration: 500000,
        // autoWebviewTimeout: 20000
    },
    baseUrl: 'http://localhost:8100/',
    onPrepare: ()=>{
        // Initialisation du bridge wd avec la configuration de Protractor
        wdBridge.initFromProtractor(exports.config);
    }
};

```

- [Référence du fichier de configuration Protractor](https://github.com/angular/protractor/blob/master/lib/config.ts)

## Le premier fichier de test E2E

Dans le fichier de configuration, nous indiquons que l'emplacement de nos tests fonctionnels est `./e2e` depuis la racine du projet.
Chaque fichier suffixé par `.e2e.js` sera considéré comme un fichier de test.

Protractor utilise par défaut le framework [Jasmine](https://jasmine.github.io/). La structure d'un fichier de test ressemble ainsi au code suivant.

```js

describe('Je m\'authentifie`, ()=> {
    // ...    

    it('Mes identifiants sont mauvais', ()=> {
        // Accès à la première page de l'application
        browser.get('/');
        // ... Réalisation du scénario de test
        expect(...).toBe(...);
    })
})
```

- [**Documentation**, Jasmine](https://jasmine.github.io/)
- [**Introduction à Jasmine**, Jasmine](https://jasmine.github.io/edge/introduction.html)

Les intéractions avec l'application sont réalisées avec Protractor. Je vous invite à parcourir le tutorial de Protractor pour découvrir comment l'utiliser :

- [**Tutorial**, Protractor](http://www.protractortest.org/#/tutorial).

## Exécuter les tests

Dans un premier temps, assurez vous que le périphérique est correctement branché à votre ordinateur de test et que le mode développeur est activé.

Nous allons compiler l'application android :

    $ ionic build android

Puis nous exécutons Appium :

    $ appium

Dès que le serveur Appium est en route, nous exécutons la suite de test avec Protractor :

    $ protractor

## Conclusion

Vous avez désormais une suite utilitaire qui vous permet d'exécuter des tests fonctionnels sur un périphérique mobile avec Protractor et Appium.
Les documentations de Protractor et Jasmine sont suffisamment détaillées pour que vous puissiez simplement écrire vos premiers tests.


## Aller plus loin

Les prochaines étapes post-installation / configurations concernent la prise en main des différents outils.

Nous vous invitons ainsi à regarder ces différentes notions.

### Utiliser le patron *Page Object*

Protractor et Selenium conseillent les utilisateurs de structurer les intéractions utilisateurs selon le patron *Page Object*. 

Le principe est simple : les intéractions avec les pages sont abstraites par un objet *page* qui décrit les actions essentielles possibles.
Ainsi, quand vous modifiez la structure d'une page de votre application, vous n'avez plus besoin de modifier le code de l'ensemble de vos tests, seulement celui de votre objet *page*. 

- [**Page Objects**, Selenium Github](https://github.com/SeleniumHQ/selenium/wiki/PageObjects)
- [**Page Objects**, Protractor test](http://www.protractortest.org/#/page-objects)

### Ecrire ses tests en Typescript

Protractor propose un exemple de projet utilisant des tests écrits en Typescript.

- [**Example Typescript**, Protractor Github](https://github.com/angular/protractor/tree/5.1.2/exampleTypescript)

### Exécuter ses tests avec Saucelabs

Si vous souhaitez industrialiser l'exécution de vos tests fonctionnels, des solutions propriétaires comme Saucelabs existent. Cette solution payante permet de tester de manière distante votre application sur un ensemble varié de périphériques et de systèmes d'exploitation.

- [**Saucelabs**](https://saucelabs.com/)


## Références 

- [**Test fonctionnel - Définition**, Crowdsourced testing](https://crowdsourcedtesting.com/fr/faq-clients/article/test-fonctionnel)
- [**Protractor**](http://www.protractortest.org/#/)
- [**Appium**](http://appium.io/)
- [**End-to-end testing an Ionic application with Appium and Protractor**](http://tombuyse.com/end-to-end-testing-an-ionic-application-with-appium-and-protractor/)
- [**E2E (End-to-End) Testing in Ionic2: An Introduction**, Josh Morony](https://www.joshmorony.com/e2e-end-to-end-testing-in-ionic-2-an-introduction/)