+++
weight = 70
date = "2017-04-23"
draft = true
title = "6. HowTo: Documenter son code Typescrit avec Compodoc"
slug = "howto-ionic-documentation"
tags = ["ionic","documentation","tuto"]
categories = [ "ionic" ]
image = "/images/howto-ionic-debug/cover.jpg"
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"           # set "main" to add this content to the main menu
+++
