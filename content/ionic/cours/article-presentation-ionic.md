+++
weight = 20
date = "2017-04-20"
draft = false
title = "1. Article : Présentation de Ionic"
slug = "article-presentation-ionic"
tags = ["ionic","cordova","presentation"]
categories = [ "ionic" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = true     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = "ionic"          # set "main" to add this content to the main menu
+++

**Ionic** est une solution logicielle permettant de faciliter la **création d'applications mobiles hybrides** et d'**application web progressive**.

**Plus qu'un simple framework**, la solution Ionic intègre **un nombre important d'outils** :

- Création et partage de prototypes
- Services clouds dédiés aux notifications push, compilations multi-plateformes distantes...
- Proposition d'une base de projet fonctionnel

...
<!--more-->

<h2> <i class="fa fa-book"></i> Définitions</h2>

### Une application native

> Application **développée spécifiquement pour un système d'exploitation** (Android, iOS...)

- **Les +** : Configuration plus fine, plus grande maîtrise du logiciel
- **Les -** : Long à développé si plusieurs systèmes visés

### Une application web

> Application développée en HTML accessible et utilisable depuis un navigateur internet.

- **Les +** : 
  - Ne nécessite qu'un navigateur web
  - Pas besoin de télécharger l'application
- **Les -** :
  - Pas d'accès au play Store
  - Plus lent qu'une application native
  - Accès restreints aux fonctions natives

### Une application hybride 

> Une application hybride est **Application web** s'exécutant sur une **webview locale** ayant des accès à une **brique native** intéragissant avec le système d'exploitation du périphérique.

- **Les +**:
  - Plus rapide à développer (un code unique pour plusieurs systèmes d'exploitation)
  - Possibilité d'utiliser du code web déjà développé
- **Les -**:
  - Plus lent qu'une application native
  - Des accès aux fonctions natives parfois compliquées

### Une application web progressive

> Une application web progressive est une **Application web multiplateforme** et fournissent des fonctionnalités aux utilisateurs se rapprochant des avantages des applications natives. Une **PWA** peut être utilisée en offline, télécharge son contenu de manière progressive (politique de cache, grâce au ***service worker***), propose une expérience utilisateur riche (*responsive*, plein écran, gestion de l'écran...) , adapte sa structure et son contenu selon la plateforme.

>Elle peut être ainsi installée sur un téléphone mobile sans avoir besoin de diffuser une application sur les *stores* traditionnels.

- **Les+**:
  - Les mêmes que pour une application web: code unique pour toutes les plateformes, code web réutilisable...
  - Peut-être téléchargée et consultée en offline
  - Plus léger qu'une application hybride ou native
- **Les-**:
  - Plus lent qu'une application native
  - Dépendant des versions des APIs web disponibles dans les navigateurs des plateformes

### Références

- [**Ionic Documentation overview, Ionic**](https://ionicframework.com/docs)
- [Native web or hybrid apps](https://www.mobiloud.com/blog/native-web-or-hybrid-apps/)
- [**Application mobile native, web ou hybride ?**, Supinfo](http://www.supinfo.com/articles/single/145-application-mobile-native-web-hybride)
- [**Application native VS Progressive Web App: que choisir ?**](https://medium.com/inside-smartapps/application-native-vs-progressive-web-app-que-choisir-e5c30c839f55)
- [**Progressive Web apps**](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Advantages)


<h2> <i class="fa fa-globe"></i> L'écosystème Ionic</h2>

### En bref

- Framework pour la création d'applications hybrides
- Racine de projet fonctionnel
- Ecosystème cloud
  - Build mobiles et publications: **Ionic AppFlow**
  - Prototypage : **Ionic Creator** et **Ionic Studio**
  - Partage de prototype : **Ionic View**

### Framework Ionic

Le **framework Ionic** fournit un ensemble de composants web adaptés pour une expérience utilisateur multi-plateformes.

Bien que l'outil ait été initialement conçu pour fonctionner avec **Angular**, il peut être utilisé en simple **JS**, **React** ou **Vue**.

Ainsi, le framework intègre aussi bien des outils CLI de génération de projet et de contenus, des composants web, une interface adaptée pour l'utilisation de composants natifs (issus de Apache Cordova ou Ionic Capacitor).

Par défaut, le style graphique proposé s'inspire de la charte graphique **Material Design**/

- Conception d'applications web, hybrides et progressive en Typescript / JS
  - Composants natifs : Ionic == surcouche **Cordova**
  - Composants web : **Angular** et composants personnalisés
- Un thème graphique initial : **Material Design**

*Exemples d'applications Ionic*

[Exemples d'applications Ionic](https://showcase.ionicframework.com/apps/top)

*Framework Ionic*
![Une Application Ionic - Cordova](/images/presentation-ionic/ionic-structure.jpg)

### Framework Ionic - Les plugins

Par défaut, une application a accès aux plugins selon les plateformes qu'elle cible.

Une petite partie des plugins natifs intègrent une compatibilité avec les navigateurs. Une attention particulière sera donnée aux plateformes compatibles de chaque plugins ajoutés.

Les applications conçues avec Ionic sont exécutées dans un navigateur web (embarqué ou non). Elles ont ainsi accès aux APIs web standards.

- Plugins natifs: Interface native / JS
  - **Le code de chaque plateforme est codé en langage natif** (un par plateforme)
  - Apache Cordova / Ionic Capacitor: Encapsulation dans des **interfaces JS**
  - Ionic : Encapsulation des interfaces Cordova / Capacitor dans des **composants (Angular, Vue...)**

- Webview
  - Utilisation des composants Angular
  - Utilisation des fonctionnalités natives du navigateur (Micro, caméra...)

### La graine de projet, déjà opérationnelle

S'appuie sur les bases Cordova + surcouche [ionic-app-script](https://github.com/driftyco/ionic-app-scripts)

- Structure initiale de projet
- Générateur de code (pages, services...)
- Tasks runner (minification, transpilation, compression, linter...)

<div class="alert alert-info" role="alert">
  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
  Les outils de test et de documentation sont à configurer manuellement
</div>

<br/>

### Prototypage : [Ionic Creator](https://creator.ionic.io)

![Capture D'écran de Ionic Creator](/images/presentation-ionic/ionic-creator.jpg)
<center>Capture D'écran de Ionic Creator*</center>

- Pratique pour **prototyper**
- **La plupart des options sont payantes** (dont la conversion HTML)
- Pas de gestion des composants personnels
- **Non OpenSource** (outil non modifiable)

### Partage de prototype : Ionic View


![Capture d'écran de Ionic View](/images/presentation-ionic/ionic-view.jpg)
<center>*Capture d'écran de Ionic View*</center>

- Développement : **partages et mises à jours de plusieurs applications**
- Pas besoin d'inscription sur les stores
- **Cas d'utilisation : retour client, collègues et testeurs**

<div class="alert alert-warning" role="alert">
  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
  Gratuit jusqu'à un certain volume d'utilisation (nombre de mobiles, volume d'upload...)
</div>


### Services Cloud

- Système de mise à jour
- Authentification centralisée avec providers Google, facebook...
- Notifications Push pour les utilisateurs
- Compilation multi-plateformes distante

<div class="alert alert-warning" role="alert">
  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
  Gratuit jusqu'à un certain volume d'utilisation (nombre de mobiles, volume d'upload...)
</div>

### Market

- Projets starters
- Plugins 
- Thèmes graphiques


<div class="alert alert-warning" role="alert">
  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
  Les licences d'utilisation ne sont pas mises en valeur, attention lors des achats !
</div>

### Conclusion

**Ionic ==**

- Framework de création d'applications mobiles hybrides et progressives
- Des bases de projet quasi-complets + utilitaires
- Des outils de prototypages
- Des services cloud (compilation, notification, authentification...)

### Références

- [Ionic Creator](https://creator.ionic.io)
- [Ionic View](https://docs.ionic.io/tools/view/)
- [Ionic Cloud](https://ionicframework.com/products)
- [Market](https://market.ionic.io)
- [Ionic Capacitor vs Apache Cordova Difference With Example — The Next Future Innovation of Hybrid Native Apps](https://medium.com/@sunilk/ionic-capacitor-vs-apache-cordova-difference-with-example-the-next-future-innovation-of-hybrid-93b1317e08cc)