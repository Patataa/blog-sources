+++
weight = 90
date = "2019-06-21"
draft = false
title = "7. Progressive Web App, Ionic et Angular"
slug = "pwa"
tags = ["ionic","pwa"]
categories = [ "ionic" ]
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"             # set "main" to add this content to the main menu
+++

Les *Progressive Web App* (PWA) sont des applications web s'appuyant sur un ensemble de technologies et de patrons standardisés leur permettant d'intégrer une expérience utilisateur web, proche d'une application native.

Les PWAs proposent une utilisation plus aisée, plus rapide et peuvent être installées afin de fournir une interface consultable, même hors ligne.

## Les caractéristiques d'une PWA

Voici quelques principes qui permettent de caractériser une web app en tant que PWA :

- On peut la trouver sur un moteur de recherche
- Elle peut être installé sur l'écran d'accueil du périphérique
- Elle peut être partagée par un lien
- Elle doit être disponible, même hors ligne
- Elle est *progressive*, cela signifie qu'elle doit gérer la rétro compatibilité avec les anciens navigateurs, tout en offrant une expérience enrichie avec les plus récents (dégradation progressive / enrichissement progressif)
- Elle peut offrir des fonctionnalités de notification (de type *push*)
- Elle fournit une interface adaptable (*responsive*)
- Elle est sécurisée, par utilisation du protocole *https* par exemple
<!--more-->

## Des exemples de PWAs

Plusieurs sites font références en tant que PWAs de qualité. Je vous invite à parcourir ces articles faisant des listes de PWAs pertinentes :

- [7 Excellent Progressive Web App Examples, by AppScope](https://medium.com/appscope/7-excellent-progressive-web-apps-that-prove-pwas-are-ready-for-mainstream-consumer-adoption-9a8a8e876eba)
- [12 Best Examples of Progressive Web Apps (PWAs) in 2019, SimiCart](https://www.simicart.com/blog/progressive-web-apps-examples/)

## Ionic et les PWA

Pour qu'une web app puisse être installée, quelques éléments sont requis :

- Un manifeste web, qui est un fichier décrivant l'application [voir un exemple](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Add_to_home_screen#Manifest)
- Un accès *Https*
- Une icône pour représenter l'application sur le périphérique (pour son accès depuis l'écran d'accueil par exemple)
- Un *service worker*

### Le manifeste web

[La documentation Mozilla explique particulièrement bien ce qu'est un manifeste Web.](https://developer.mozilla.org/fr/docs/Web/Manifest)

### Le service Worker

Les *Service Workers* sont des scripts s'exécutant en arrière plan, séparés de la page principale d'une application.
Il peuvent s'inscrire en tant que proxy virtuel entre l'application web et le réseau internet afin de fournir une expérience hors-ligne. 

Ceci fonctionne grâce à des fonctionnalités permettant de mettre en cache une partie du contenu récupéré depuis des APIs distantes.

Les *Services Workers* sont exécutés dans un thread différent de celui utilisé pour l'application principal. Cela signifie qu'ils n'ont pas accès au DOM et nécessitent donc une approche particulière pour transmettre les données.

Attention cependant, les *Services Workers* doivent être exécutés dans un environnement sécurisé (HTTPS) !

## Et Ionic / Angular dans tout ça ?

**Ionic4+** s'appuyant sur **Angular** propose une démarche adaptée et simplifiée pour transformer votre application en PWA.
A l'aide du package `@angular/pwa`, quelques options de configuration permettent de mettre en place le manifeste web, d'inscrire rapidement un *service worker* et de communiquer avec lui.

Pour ça, je vous invite à lire la documentation officielle qui explique tout en détail.

- [Publishing a Progressive Web App: ionic framework](https://ionicframework.com/docs/publishing/progressive-web-app)
- [Angular: Getting started with service workers](https://angular.io/guide/service-worker-getting-started)
- [Angular: Service Worker Communication](https://angular.io/guide/service-worker-communications)
- [Angular: Service worker configuration](https://angular.io/guide/service-worker-config)
- [Angular: Service worker in production](https://angular.io/guide/service-worker-devops)

Avec l'utilisation de `ng add @angular/pwa`, la CLI **Angular** va ainsi automatiser un ensemble d'actions facilitant la transformation en PWA.

Les packages nécessaires seront installés et ajoutés, le service worker sera activé, le fichier `index.html` sera mis à jour afin d'intégrer le manifeste et les tags pour la couleur de l'application, les premières icônes seront mises en place, et le fichier de configuration du service worker sera instancié.

En outre, **Angular** propose une interface particulière pour faciliter la communication entre votre application principale et le service worker, grâce au service *SwUpdate*.

La gestion des URLs à mettre en cache est simplifiée à l'aide du fichier de configuration `src/ngsw-config.json`.

Tous les détails sont expliqués dans la documentation.

## Références

- [Introduction to progressive web apps](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Introduction)
- [How to make PWAs installable](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Installable_PWAs)
- [Making PWAs work offline with Service workers](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Offline_Service_workers)
- [Service Workers: an Introduction](https://developers.google.com/web/fundamentals/primers/service-workers/)
- [Angular and Service worker: introduction](https://angular.io/guide/service-worker-intro)