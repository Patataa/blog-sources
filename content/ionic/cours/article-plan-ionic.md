+++
weight = 10
date = "2017-04-19"
draft = true
title = "0. Plan : Ionic"
slug = "plan-ionic"
tags = ["ionic","bases","presentation"]
categories = [ "ionic" ]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = "ionic"           # set "main" to add this content to the main menu
+++

Le plan du cours de Ionic, avec les différentes sections rédigées / en cours de rédaction.

<!--more-->

## [0. Les attentes du cours](attentes-ionic)
- Description du livrable
- Présentation orale
- Partiel court sur table

## [1. Présentation de Ionic](/post/article-presentation-ionic) 

- Définitions
- L'écosystème Ionic

## [2. Présentation d'Angular](/post/article-presentation-angular)

- En bref
- Une architecture MV*
- Les composants
- Les services
- Les modules

## [3. Démarrer un projet avec Ionic](/post/projet-ionic-bases)
- Présentation de Ionic CLI
    - Création de projet
    - Génération de code
    - Exécution de l'application
    - Déploiement
- La structure d'un projet
- Ajouter une page (step by step)
    - Code CLI
    - Création de variables avec Binding
    - Ajout d'un composant Natif (Son ?) / Installation
- La navigation
    - La pile d'état
    - Changer d'écran
    - Utiliser des onglets

## [4. Débugger un projet Ionic avec Chrome](/post/howto-ionic-debug)


## Les tests unitaires et fonctionnels
- L'approche BDD
- Protractor
    - Protractor : la surcouche Selenium
    - Ecrire un scénario de test
    - Le Page Pattern
- Karma et Jasmine
    - Karma : un test runner
    - Jasmine : un framework de test
    - Configuration type de Karma
    - Ecrire un test avec Jasmine
    - Mocker son application
    - Les fixtures

## Documentation
- Typescript Documentation
- Génération de la documentation auto
	
## Politique de stockage
- Utilisation de SQL Lite
- Persistance: PouchDB avec SQL Lite
- Utilisation des fichiers locaux

## Personnaliser l'apparence de son application
- Les variables SCSS
- Politique de nommage
- Les thèmes
