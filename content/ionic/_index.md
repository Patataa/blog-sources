+++
title = "Ionic"
weight=5
+++

## À propos

Ce cours est à destination des Licences Pro web de Clermont-Ferrand.
Le contenu présenté ici n'est pas exhaustif et ne pourra être considéré comme étalon minimal de connaissance nécessaire au partiel final.

<div class="alert alert-info">

    Les étudiants sont fortement invités à consolider leurs connaissances avec les lectures des références précisées au sein de chaque slide / support.
</div> 