+++
weight = 70
date = "2018-06-04"
draft = false
title = "Questionnaire Satisfaction"
slug = "satisfaction"
tags = ["ionic","satisfaction"]
categories = [ "ionic", "exam" ]
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
+++

Vous trouverez ci-dessous le questionnaire de satisfaction concernant le cours Web Tranverse.
Ces quelques informations me permettront d'adapter les prochains cours.

Je compte sur votre bienveillance et votre franchise.

Merci beaucoup pour le temps accordé.

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdWp6GbUEf84KLWRGkE2g5RdRlasUvo9buGWZYz12Zmp8m3dw/viewform?embedded=true" width="100%" height="900" frameborder="0" marginheight="0" marginwidth="0">Chargement en cours...</iframe>