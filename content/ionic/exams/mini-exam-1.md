+++
weight = 70
date = "2018-06-04"
draft = false
title = "Mini exam - 1"
slug = "mini-exam-1"
tags = ["ionic","exam"]
categories = [ "ionic", "exam" ]
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
+++

Vous trouverez ci-dessous le sujet du premier mini-exam.
Vous avez 10 minutes top chrono pour le réaliser, une seule participation par personne (la première soumise sera la seule prise en compte).
Les soumissions seront refusées passées le délai des 10 premières minutes.

Bon courage !

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfPXv7KjqBaucAfPSNk2dKcHs75moJsgNabUR5Lu4NjLt0TmQ/viewform?embedded=true" width="100%" height="900" frameborder="0" marginheight="0" marginwidth="0">Chargement en cours...</iframe>