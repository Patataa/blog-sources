+++
weight = 70
date = "2018-06-04"
draft = false
title = "Mini exam - 2"
slug = "mini-exam-2"
tags = ["ionic","exam", "second"]
categories = [ "ionic", "exam" ]
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
+++

Vous trouverez ci-dessous le sujet du deuxième mini-exam.
Vous avez 10 minutes top chrono pour le réaliser, une seule participation par personne (la première soumise sera la seule prise en compte).
Les soumissions seront refusées passées le délai des 10 premières minutes.

Bon courage !

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdtLda8UHhmUSrRzW4qzAVFCUNeV9YaDSwCARb7vnmuR0iYJA/viewform?embedded=true" width="100%" height="900" frameborder="0" marginheight="0" marginwidth="0">Chargement en cours...</iframe>
