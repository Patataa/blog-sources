+++
weight = 70
date = "2017-04-23"
draft = false
title = "Partiel 2017"
slug = "partiel-2017"
tags = ["ionic","exam"]
categories = [ "ionic" ]
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
menu = "ionic"           # set "main" to add this content to the main menu
+++

# Web Transverse - Examen

## Questions de cours / 10

- Quelles sont les différences entre une application native, une application web et une application hybride ?
- En quelques mots, expliquez ce que sont ces outils :  
    - Cordova
    - Angular
    - Ionic
- Expliquez ce qu'est un composant et ce qui le différencie d'une directive.
- Expliquez ce qu'est un service et ses cas d'utilisation.
- Expliquez la gestion des dépendances et leur injection dans Angular.

## Cas pratique / 10

Dans le cadre du développement d'une page pour une application **Ionic**, nous souhaitons proposer l'affichage dynamique du niveau de batterie.

La page se compose de deux informations différentes : l'état actuel de batterie et l'historique du changement de niveau (date et niveau de batterie).

Vous trouverez ci-dessous la structure de code attendue, les contraintes fonctionnelles et la documentation du composant natif.

Proposez ainsi une implémentation du module dédié.

### Structure attendue

- Un modèle `BatteryState` qui permet d'associer une valeur de batterie à une date
- Un service `SaveBatteryStatus` qui enregistre en local en tant qu'attribut la date de chaque changement de batterie
- Un composant `BatteryPage` qui se compose de deux éléments : l'affichage dynamique du niveau de batterie et l'affichage de l'historique de changement de niveau de batterie sous forme de liste.
- Un module `DisplayBatteryModule` exposant le service `SaveBatteryStatus` et le composant `BatteryPage`

### Contraintes fonctionnelles

```gherkin
Fonctionnalité: Affichage dynamique du niveau de batterie et son historique de changement
    
    Scénario:   Le niveau de batterie change
        Soit    L'écran d'affichage de batterie ouvert
        Quand   Le niveau de batterie change
        Alors   Une sauvegarde locale de la date du changement (`new Date()`) est réalisée
        Et      La valeur du niveau de batterie est mise à jour
        Et      L'historique est mis à jour avec la nouvelle date


    Scénario:   Le niveau de batterie devient bas
        Soit    L'écran d'affichage de batterie ouvert
        Quand   Le niveau de batterie devient bas
        Alors   Un encadré orange s'affiche indiquant que mon niveau de batterie
                est bas
        Et      Le texte indiquant le niveau de batterie devient orange
        Et      La valeur du niveau de batterie est mise à jour
        Et      L'historique est mis à jour avec la nouvelle date

    Scénario:   Le niveau de batterie devient critique
        Soit    L'écran d'affichage de batterie ouvert
        Quand   Le niveau de batterie devient critique
        Alors   Un encadré rouge s'affiche indiquant que mon niveau de batterie
                est bas
        Et      Le texte indiquant le niveau de batterie devient rouge
        Et      La valeur du niveau de batterie est mise à jour
        Et      L'historique est mis à jour avec la nouvelle date
```

### Documentation

#### JS/TS : `Date`

Get the current date : `let date = new Date()`

#### Angular: `DatePipe`

**What it does**    Formats a date according to locale rules.  
**How to use**      `date_expression | date[:format]`  
**NgModule**        `CommonModule`  
**Description**

- `expression` is a date object or a number (milliseconds since UTC epoch) or an ISO string 
- `format` indicates which date/time components to include. The format can be predefined as shown below or custom as shown in the table.
    - `'medium'`: equivalent to 'yMMMdjms' (e.g. Sep 3, 2010, 12:05:08 PM for en-US)
    - `'short'`: equivalent to 'yMdjm' (e.g. 9/3/2010, 12:05 PM for en-US)
    - `'fullDate'`: equivalent to 'yMMMMEEEEd' (e.g. Friday, September 3, 2010 for en-US)
    - `'longDate'`: equivalent to 'yMMMMd' (e.g. September 3, 2010 for en-US)
    - `'mediumDate'`: equivalent to 'yMMMd' (e.g. Sep 3, 2010 for en-US)
    - `'shortDate'`: equivalent to 'yMd' (e.g. 9/3/2010 for en-US)
    - `'mediumTime'`: equivalent to 'jms' (e.g. 12:05:08 PM for en-US)
    - `'shortTime'`: equivalent to 'jm' (e.g. 12:05 PM for en-US)


*Reference* : **Angular Documentation**,  https://angular.io/docs/ts/latest/api/common/index/DatePipe-pipe.html
#### Ionic: `Battery Status`

##### Instance Members

`onChange()`  
Watch the change in battery level  
**Returns:** `Observable<BatteryStatusResponse>` Returns an observable that pushes a status object

`onLow()`  
Watch when the battery level goes low  
**Returns:** `Observable<BatteryStatusResponse>` Returns an observable that pushes a status object

`onCritical()`  
Watch when the battery level goes to critial  
**Returns:** `Observable<BatteryStatusResponse>` Returns an observable that pushes a status object

##### BatteryStatusResponse

| Param	    | Type	    | Details|
|---|---|---|
| level  	| `number`	| The battery charge percentage |
| isPlugged	| `boolean` | A boolean that indicates whether the device is plugged in |

*Reference* : **Ionic Documentation**,  http://ionicframework.com/docs/native/battery-status/

#### RxJS: `Observable<T>.subscribe()`

`Observable<T>.subscribe([observer] | [onNext], [onError], [onCompleted])`

Subscribes an observer to the observable sequence.

**Arguments**

- `[observer] (Observer)`: The object that is to receive notifications.
- `[onNext] (Function)`: Function to invoke for each element in the observable sequence.
- `[onError] (Function)`: Function to invoke upon exceptional termination of the observable sequence.
- `[onCompleted] (Function)`: Function to invoke upon graceful termination of the observable sequence.

**Returns**

- `(Disposable):` The source sequence whose subscriptions and unsubscriptions happen on the specified scheduler.

*Reference*: **RxJS Documentation**, https://github.com/Reactive-Extensions/RxJS/blob/master/doc/api/core/operators/subscribe.md