+++
weight = 150
date = "2019-06-30"
draft = false
title = "Rendu du projet Ionic 2019"
slug = "rendu-projet-ionic"
tags = ["ionic","rendu", "projet"]
categories = [ "ionic", "exam" ]
comments = true     # set false to hide Disqus comments
share = true        # set false to share buttons
+++

Vous trouverez ci-dessous le formulaire de soumission du projet Ionic.

Veuillez à vous assurer de l'accessibilité de votre projet git.

Bon courage et bonne continuation !

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfQf7dmWeDSTh5yVtD3Qq8nkTA3d-iUijq1ShRox52Jsv6rqg/viewform?embedded=true" width="100%" height="900" frameborder="0" marginheight="0" marginwidth="0">Chargement en cours...</iframe>