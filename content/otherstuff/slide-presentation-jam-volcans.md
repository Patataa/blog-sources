+++
date = "2018-10-20"
draft = false
title = "Jam Des Volcans 2019 - Présentation"
excerpt = "Slides : Ionic 2+ - Retour d'expérience"
slug = "ijam-des-volcans-presentation"
tags = ["jamdesvolcans","jam","presentation"]
image = "http://jam-des-volcans.fr//header/banner.jpg"
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = ""           # set "main" to add this content to the main menu
+++

[Lien vers la présentation](/pdf/JamDesVolcans_Slides.pdf)
