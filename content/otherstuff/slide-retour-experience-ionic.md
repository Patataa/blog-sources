+++
date = "2017-04-21"
draft = false
title = "Ionic 2+ - Retour d'expérience"
excerpt = "Slides : Ionic 2+ - Retour d'expérience"
slug = "ionic-retour-experience"
tags = ["ionic","cordova","presentation"]
image = "https://ionicframework.com/img/ionic-meta.jpg"
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = ""           # set "main" to add this content to the main menu
+++

[Lien vers la présentation](/pdf/clermontech-retour-exp-ionic.pdf)
