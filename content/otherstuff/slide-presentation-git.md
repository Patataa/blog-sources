+++
date = "2016-12-10"
draft = false
title = "Slides : Présentation de Git"
excerpt = "Slides : Présentation brève de Git"
slug = "slide-presentation-git"
tags = ["git","presentation"]
categories = [ "slide" ]
comments = false     # set false to hide Disqus comments
share = false        # set false to share buttons
menu = ""           # set "main" to add this content to the main menu
type= "slide"
theme = "white"
[revealOptions]
transition= 'concave'
controls= true
progress= true
history= true
center= true
+++

<section data-transition="slide" data-background="#B8312F" data-background-transition="concave">		
                    
    <h1> Présentation de <i class="fa fa-git" aria-hidden="true"></i></h1>
    <blockquote>Ou comment dormir la nuit lors de soirées projets :)</blockquote>
    <p>Cédric CHARIERE FIEDLER</p>
    <p><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>  <a href="http://charierefiedler.github.io/slide/slide-presentation-git" style="color:white;"/>http://charierefiedler.github.io/slide/slide-presentation-git</p>
    
</section>
                
---

# Question Time!

>Comment synchronisez vous vos fichiers lors d'un travail en équipe ?

  - Zip avec la date dans le nom
  - Clé USB, Boîte mail
  - Google drive

---

# Douleurs, malheurs, souffrance, peine et mort ...

---

## Installer <i class="fa fa-git" aria-hidden="true"></i>

  Linux

  `$sudo apt-get install git`

  Windows

  DL || `choco install git` 

---

## Comment ça marche ?

  - Notion d'un repo
  - Fichier local / distant

---

## Création d'un repo local

> Ici s'arrête la souffrance !

`$git init`

---

### Traquons le fichier !

`$git add <mon-fichier>`

`$git rm <mon-fichier>`
---

## J'ai fait une modif, je veux l'enregistrer !

  `$git commit -am "Mon Message ! Muhahahahaha"`

---

## En fait, je me suis trompé...

   `$git reset <numero-commit> <mon-fichier>`

---

## Bon, je souhaite récupérer le travail des autres

  `$git fetch / git pull`

---

## Merge Time!

  `$git diff`

De bons outils pour faire la diff ?

---

## Se connecter à un repo distant

1. Création d'un repository : **Github**, **Gitlab**, **Bitbucket**
2. Connexion du repository local avec le distant

`$git remote add <label-du-repo-distant> <url>`

---

## Envoie des modifs

  `$git push -u`

---

## Je veux mettre de côté mon taf avant de récuperer le boulot des autres

  `$git stash`

  `$git stash pop`

---

## Qu'est ce qu'une branche ?

  `$git checkout -b myBranch`

- A quoi ça sert ?

---

## Notion de GitFlow :

- Dev, Master, fix

---

## Un peu de magie ? Les hooks !

- Qu'est ce qu'un Hook, à quoi ça sert ?

---

## Un essai en vrai ?

[https://try.github.io/levels/1](https://try.github.io/levels/1)

---

# Des questions ?